#include "StdInc.h"

namespace Blaze
{
	TdfBinary::TdfBinary(std::string label, int value)
		: Tdf(TDF_TYPE_BINARY)
	{
		mLabel = label;
		mValue = value;
	}

	void TdfBinary::operator=(int pTdf)
	{
		mValue = pTdf;
	}

	int TdfBinary::operator=(TdfBinary* pTdf)
	{
		return ((TdfBinary*)pTdf)->mValue;
	}
}