#include "StdInc.h"

namespace Blaze
{
	TdfUnion::TdfUnion(std::string label, int value)
		: Tdf(TDF_TYPE_UNION)
	{
		mLabel = label;
		mValue = value;
	}

	void TdfUnion::operator=(int pTdf)
	{
		mValue = pTdf;
	}

	int TdfUnion::operator=(TdfUnion* pTdf)
	{
		return ((TdfUnion*)pTdf)->mValue;
	}
}