#include "StdInc.h"

namespace Blaze
{
	TdfEncoder::TdfEncoder(Stream* stream)
		: mStream(stream)
	{

	}

	TdfEncoder::~TdfEncoder()
	{
		//delete mStream;
	}

	void TdfEncoder::WriteString(const char* pContent)
	{
		int pLength = strlen(pContent);

		// write string length
		WriteInteger(pLength + 1);
		
		// write string content
		mStream->WriteString(pContent, pLength);

		// write null byte
		mStream->WriteByte(0);
	}

	void TdfEncoder::WriteInteger(int pInt)
	{
		std::list<uint8_t> bytes;

		if (pInt < 0x40)
		{
			bytes.push_back((uint8_t)(pInt & 0xFF));
		}
		else
		{
			bytes.push_back((uint8_t)((pInt & 0x3F) | 0x80));
			long currshift = pInt >> 6;

			while (currshift >= 0x80)
			{
				bytes.push_back((uint8_t)((currshift & 0x7F) | 0x80));
				currshift >>= 7;
			}

			bytes.push_back((uint8_t)currshift);
		}

		for (uint8_t byte : bytes)
		{
			mStream->WriteByte(byte);
		}
	}

	void TdfEncoder::WriteUnion(TdfUnion* pTdf)
	{
		// write union byte
		WriteInteger(pTdf->mValue);

		for (auto& tdf : pTdf->mStruct)
		{
			WriteTdf(tdf.second);
		}
	}

	void TdfEncoder::WriteStruct(TdfStruct* pTdf)
	{
		for (auto& tdf : pTdf->mStruct)
		{
			WriteTdf(tdf.second);
		}

		// write null byte
		mStream->WriteByte(0);
	}

	void TdfEncoder::WriteList(TdfList* pTdf)
	{
		mStream->WriteByte(pTdf->mListType); // type, int list = 0?, string list = 1, struct list = 3, XYZ list = 9?
		mStream->WriteByte(pTdf->mList.size());

		for (int i = 0; i < pTdf->mList.size(); i++)
		{
			switch (pTdf->mListType)
			{
			case 0:
				WriteInteger((int)pTdf->mList[i]);
				break;

			case 1:
				WriteString((const char*)pTdf->mList[i]);
				break;

			case 3:
				WriteStruct((TdfStruct*)pTdf->mList[i]);
				break;
			}
		}
	}

	void TdfEncoder::WriteMap(TdfMap* pTdf)
	{
		// write list types
		mStream->WriteByte(pTdf->mListType1);
		mStream->WriteByte(pTdf->mListType2);

		// write size of first or second list
		WriteInteger(pTdf->mList1.size());

		for (int i = 0; i < pTdf->mList1.size(); i++)
		{
			switch (pTdf->mListType1)
			{
			case 0:
				WriteInteger((int)pTdf->mList1[i]);
				break;

			case 1:
				WriteString((const char*)pTdf->mList1[i]);
				break;

			case 3:
				WriteStruct((TdfStruct*)pTdf->mList1[i]);
				break;
			}

			switch (pTdf->mListType2)
			{
			case 0:
				WriteInteger((int)pTdf->mList1[i]);
				break;

			case 1:
				WriteString((const char*)pTdf->mList2[i]);
				break;

			case 3:
				WriteStruct((TdfStruct*)pTdf->mList2[i]);
				break;
			}
		}
	}

	void TdfEncoder::WriteTdf(Tdf* pTdf)
	{
		// write Tdf tag (label) and type
		mStream->WriteTag(pTdf->mLabel);
		mStream->WriteByte(pTdf->mType);	

		// write Tdf content
		switch (pTdf->mType)
		{
		//case TDF_TYPE_MIN:
		//	break;

		case TDF_TYPE_INTEGER:
			WriteInteger(((TdfInteger*)pTdf)->mInt);
			break;

		case TDF_TYPE_BINARY:
			WriteInteger(((TdfBinary*)pTdf)->mValue);
			break;

		case TDF_TYPE_UNION:
			WriteUnion((TdfUnion*)pTdf);
			break;

		case TDF_TYPE_STRING:
			WriteString(((TdfString*)pTdf)->c_str());
			break;

		case TDF_TYPE_STRUCT:
			WriteStruct((TdfStruct*)pTdf);
			break;

		case TDF_TYPE_LIST:
			WriteList((TdfList*)pTdf);
			break;

		case TDF_TYPE_MAP:
			WriteMap((TdfMap*)pTdf);
			break;
		}
	}
}