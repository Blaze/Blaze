#pragma once

namespace Blaze
{
	class TdfInteger : public Tdf
	{
	public:
		int mInt;

		TdfInteger(std::string label, int value);

		int operator=(TdfInteger* pTdf);
		void operator=(int pTdf);
	};
}