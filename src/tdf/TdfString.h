#pragma once

namespace Blaze
{
	class TdfString : public Tdf
	{
	private:
		char* mTdfStringPointer;
		char mMemGroup;
		char mOwned : 1;
		char mDeepCopy : 1;

	public:
		int mLength;
		
		TdfString(std::string label, std::string value);
		TdfString(char memGroupId);
		TdfString(TdfString* rhs, char memGroupId);
		TdfString(const char* value, char memGroupId);
		~TdfString();

		void release();
		void set(const char* value, int len);
		void assignBuffer(char* buffer);
		char* copyToBuffer(const char* inputString, char* buffer);

		int length();
		const char* c_str();

		bool operator<(TdfString* rhs);
		TdfString* operator=(const char* rhs);
		TdfString* operator=(TdfString* rhs);
	};
}