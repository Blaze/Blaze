#include "StdInc.h"

namespace Blaze
{
	TdfList::TdfList(std::string label, uint8_t type)
		: Tdf(TDF_TYPE_LIST)
	{
		mLabel = label;

		mListType = type;
	}
}