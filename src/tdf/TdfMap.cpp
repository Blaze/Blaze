#include "StdInc.h"

namespace Blaze
{
	TdfMap::TdfMap(std::string label, uint8_t listType1, uint8_t listType2)
		: Tdf(TDF_TYPE_MAP)
	{
		mLabel = label;

		mListType1 = listType1;
		mListType2 = listType2;
	}
}