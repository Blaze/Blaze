#pragma once

namespace Blaze
{
	class TdfStruct : public Tdf
	{
	public:
		TdfStruct();
		TdfStruct(std::string label);

		std::map<std::string, Tdf*> mStruct;
	};
}