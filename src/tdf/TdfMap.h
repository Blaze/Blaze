#pragma once

namespace Blaze
{
	class TdfMap : public Tdf
	{
	public:
		TdfMap(std::string label, uint8_t listType1, uint8_t listType2);

		std::map<int, void*> mList1;
		std::map<int, void*> mList2;

		uint8_t mListType1;
		uint8_t mListType2;
	};
}