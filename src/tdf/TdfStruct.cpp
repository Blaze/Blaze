#include "StdInc.h"

namespace Blaze
{
	TdfStruct::TdfStruct()
		: Tdf(TDF_TYPE_STRUCT)
	{
		mLabel = "";
	}

	TdfStruct::TdfStruct(std::string label)
		: Tdf(TDF_TYPE_STRUCT)
	{
		mLabel = label;
	}
}