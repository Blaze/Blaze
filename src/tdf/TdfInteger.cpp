#include "StdInc.h"

namespace Blaze
{
	TdfInteger::TdfInteger(std::string label, int value)
		: Tdf(TDF_TYPE_INTEGER)
	{
		mLabel = label;
		mInt = value;
	}

	void TdfInteger::operator=(int pTdf)
	{
		mInt = pTdf;
	}

	int TdfInteger::operator=(TdfInteger* pTdf)
	{
		return ((TdfInteger*)pTdf)->mInt;
	}
}