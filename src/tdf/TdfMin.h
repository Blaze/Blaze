#pragma once

namespace Blaze
{
	class TdfMin : public Tdf
	{
	public:
		uint8_t mValue;

		TdfMin(std::string label, uint8_t value);

		uint8_t operator=(TdfMin* pTdf);
		void operator=(uint8_t pTdf);
	};
}