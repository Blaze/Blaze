#include "StdInc.h"

namespace Blaze
{
	TdfString::TdfString(std::string label, std::string value)
		: Tdf(TDF_TYPE_STRING)
	{
		mLabel = label;

		this->set(value.c_str(), value.length());
	}

	TdfString::TdfString(char memGroupId)
		: Tdf(TDF_TYPE_STRING)
	{
		mTdfStringPointer = 0;
		mLength = 0;
		mMemGroup = memGroupId;
		mOwned &= 0xFE | 2;
	}

	TdfString::TdfString(TdfString* rhs, char memGroupId)
		: Tdf(TDF_TYPE_STRING)
	{
		mTdfStringPointer = 0;
		mMemGroup = memGroupId;
		mOwned &= 0xFE | 2;

		if (rhs->mLength)
		{
			if (rhs->mOwned & 2)
			{
				mTdfStringPointer = (char*)malloc(rhs->mLength + 1);

				mOwned |= 1;
				mLength = rhs->mLength;

				return;
			}

			mTdfStringPointer = rhs->mTdfStringPointer;
			mOwned &= (0xFE | 2) & 0xFD;
		}

		mLength = rhs->mLength;
	}

	TdfString::TdfString(const char* value, char memGroupId)
		: Tdf(TDF_TYPE_STRING)
	{
		mOwned &= 0xFE | 2;

		mTdfStringPointer = 0;
		mLength = 0;
		mMemGroup = memGroupId;

		this->set(value, 0);
	}

	TdfString::~TdfString()
	{
		this->release();
	}

	void TdfString::release()
	{
		if (mOwned & 1)
		{
			free(mTdfStringPointer);
			mOwned &= 0xFE;
		}

		mLength = 0;
		mTdfStringPointer = 0;
	}

	char* TdfString::copyToBuffer(const char* inputString, char* buffer)
	{
		if (!buffer)
		{
			return 0;
		}

		if (mOwned & 1)
		{
			free(mTdfStringPointer);
			mOwned &= 0xFE;
		}

		mLength = strlen(inputString);

		if (mLength != -1)
		{
			if (!inputString)
			{
				*buffer = 0;
				mTdfStringPointer = buffer;

				return &buffer[mLength + 1];
			}

			strncpy(buffer, inputString, mLength + 1);
			buffer[mLength] = 0;
		}

		mTdfStringPointer = buffer;
		return &buffer[mLength + 1];
	}

	int TdfString::length()
	{
		return mLength;
	}

	const char* TdfString::c_str()
	{
		return mTdfStringPointer;
	}

	void TdfString::set(const char* value, int len)
	{
		mTdfStringPointer = (char*)malloc(len);

		memcpy(mTdfStringPointer, value, len);
		mTdfStringPointer[len] = 0;
		mLength = len;
	}

	void TdfString::assignBuffer(char* buffer)
	{
		if (mOwned & 1)
		{
			free(mTdfStringPointer);
			mOwned &= 0xFE;
		}

		if (buffer)
		{
			mTdfStringPointer = buffer;
			mLength = strlen(buffer);
		}
		else
		{
			mTdfStringPointer = 0;
			mLength = 0;
		}
	}

	bool TdfString::operator<(TdfString* rhs)
	{
		return strcmp(mTdfStringPointer, rhs->mTdfStringPointer) < 0;
	}

	TdfString* TdfString::operator=(const char* rhs)
	{
		this->set(rhs, 0);
		return this;
	}

	TdfString* TdfString::operator=(TdfString* rhs)
	{
		this->set(rhs->mTdfStringPointer, rhs->mLength);
		return this;
	}
}