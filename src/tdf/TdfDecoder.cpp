#include "StdInc.h"

namespace Blaze
{
	TdfString* TdfDecoder::ReadString(Stream* pStream, std::string label)
	{
		int length = 0;
		std::string data = "";

		// Create new Tdf string
		TdfString* pTdfString = new TdfString(0x69);

		// Read string data
		length = ReadInteger(pStream); // ReadInteger?
		pStream->ReadString(data, length - 1);
		
		// Copy string data to Tdf string
		pTdfString->mLabel = label;
		pTdfString->set(data.c_str(), length - 1);

#ifdef DECODER_DEBUG
		printf("[str] %s: %s\n", label.c_str(), data.c_str());
#endif
		
		return pTdfString;
	}

	int TdfDecoder::ReadInteger(Stream* pStream)
	{
		int res = 0;

		if (pStream->ReadByte((uint8_t*)&res) && res >= 0x80)
		{
			res &= 0x3F;
			uint8_t b = 0xFF;

			for (int i = 1; i < 8; i++)
			{
				if (pStream->ReadByte(&b))
				{
					res |= (int)(b & 0x7F) << ((i * 7) - 1);

					if (b < 0x80)
					{
						break;
					}
				}
			}
		}

#ifdef DECODER_DEBUG
		printf("[int] ?: %s\n", ((std::string)std::to_string(res)).c_str());
#endif

		return res;
	}

	TdfInteger* TdfDecoder::ReadInteger(Stream* pStream, std::string label)
	{
		int res = 0;

		if (pStream->ReadByte((uint8_t*)&res) && res >= 0x80)
		{
			res &= 0x3F;
			uint8_t b = 0xFF;

			for (int i = 1; i < 8; i++)
			{
				if (pStream->ReadByte(&b))
				{
					res |= (int)(b & 0x7F) << ((i * 7) - 1);
				
					if (b < 0x80)
					{
						break;
					}
				}
			}
		}

#ifdef DECODER_DEBUG
		printf("[int] %s: %s\n", label.c_str(), ((std::string)std::to_string(res)).c_str());
#endif

		return new TdfInteger(label, res);
	}

	TdfMin* TdfDecoder::ReadMin(Stream* pStream, std::string tag)
	{
		uint8_t value = 0;

		pStream->ReadByte(&value);

		return new TdfMin(tag, value);
	}

	TdfUnion* TdfDecoder::ReadUnion(Stream* pStream, std::string label)
	{
		int res = 0;

		if (pStream->ReadByte((uint8_t*)&res) && res >= 0x80)
		{
			res &= 0x3F;
			uint8_t b = 0xFF;

			for (int i = 1; i < 8; i++)
			{
				if (pStream->ReadByte(&b))
				{
					res |= (int)(b & 0x7F) << ((i * 7) - 1);

					if (b < 0x80)
					{
						break;
					}
				}
			}
		}

#ifdef DECODER_DEBUG
		printf("[union] %s: %s\n", label.c_str(), ((std::string)std::to_string(res)).c_str());
#endif

		TdfUnion* pUnion = new TdfUnion(label, res);

		while (*(uint8_t*)&pStream->m_data[pStream->m_readPos] != 0)
		{
			ReadTdf(pStream, pUnion->mStruct);
		}

		uint8_t byte = 0;
		pStream->ReadByte(&byte);

		return pUnion;
	}

	TdfStruct* TdfDecoder::ReadStruct(Stream* pStream, std::string label)
	{
		TdfStruct* pStruct = new TdfStruct(label);

		while (*(uint8_t*)&pStream->m_data[pStream->m_readPos] != 0)
		{
			ReadTdf(pStream, pStruct->mStruct);
		}

		pStruct->mLabel = label;

#ifdef DECODER_DEBUG
		printf("[struct] %s %i\n", label.c_str(), pStruct->mStruct.size());
#endif

		return pStruct;
	}

	TdfList* TdfDecoder::ReadList(Stream* pStream, std::string label)
	{
		uint8_t listType = 0;
		uint8_t listSize = 0;

		pStream->ReadByte(&listType);
		pStream->ReadByte(&listSize);

		TdfList* pList = new TdfList(label, listType);

		int num = 0;
		std::string str = "";
		int strlen = 0;

		for (int i = 0; i < listSize; i++)
		{
			switch (listType)
			{
			// integer
			case 0:
				pList->mList[i] = (void*)ReadInteger(pStream);
				break;

			// string
			case 1:
				strlen = ReadInteger(pStream);
				pStream->ReadString(str, strlen);

				pList->mList[i] = (void*)str.c_str();
				break;

			// struct
			case 3:
				// this is one of the 'oh fuck, what should i do now' moments.
				// oh wait, this isn't hard at all.
				pList->mList[i] = (TdfStruct*)ReadStruct(pStream, ReadTag(pStream));
				break;

			case 9:
				// Union3, XYZ
				break;
			};
		}

		pList->mLabel = label;

#ifdef DECODER_DEBUG
		printf("[list] %s\n", label.c_str());
#endif

		return pList;
	}

	/* TdfUnion* TdfParser::ParseUnion(Stream* pStream, std::string& label)
	{
		
	} */

	// Credits to Pedro Martins
	std::string TdfDecoder::ReadTag(Stream* pStream)
	{
		int tag = 0;
		std::string label = "";

		pStream->Read(&tag, 3);

		tag = _byteswap_ulong(tag) >> 8;

		label += (((tag >> 18) & 0x3F) & 0x1F) | 64;
		label += (((tag >> 12) & 0x3F) & 0x1F) | 64;
		label += (((tag >> 6) & 0x3F) & 0x1F) | 64;
		label += ((tag & 0x3F) & 0x1F) | 64;

		return label;
	}

	std::string TdfDecoder::ReadTag(Stream* pStream, bool* isMin)
	{
		int tag = 0;
		std::string label = "";

		pStream->Read(&tag, 3);

		// MIN and INTEGER tdf's have the same type (which is 0)
		// so they hide flags in the tags so we can distinguish
		// whether we're dealing with MIN's or INTEGER's.
		// 0 = integer
		// 1 = min

		if (tag & 1)
		{
			*isMin = true;
		}
		else
		{
			*isMin = false;
		}

		tag = _byteswap_ulong(tag) >> 8;

		label += (((tag >> 18) & 0x3F) & 0x1F) | 64;
		label += (((tag >> 12) & 0x3F) & 0x1F) | 64;
		label += (((tag >> 6) & 0x3F) & 0x1F) | 64;
		label += ((tag & 0x3F) & 0x1F) | 64;

		return label;
	}

	void TdfDecoder::ReadTdf(Stream* pStream, std::map<std::string, Tdf*>& tdf)
	{
		TdfBaseType type;
		std::string label = "";
		
		// read tag
		bool isMin = false;
		label = ReadTag(pStream, &isMin);

		// read type
		pStream->ReadByte((uint8_t*)&type);

		// set proper type
		if (type == TDF_TYPE_INTEGER && isMin)
		{
			type = TDF_TYPE_MIN;
		}

		printf("LABEL IS %s\n", label.c_str());
		
		switch (type)
		{
		case TDF_TYPE_INTEGER:
			if (isMin)
			{
				tdf.insert(std::make_pair(label, ReadMin(pStream, label)));
			}
			else
			{
				tdf.insert(std::make_pair(label, ReadInteger(pStream, label)));
			}

			break;

		case TDF_TYPE_STRING:
			tdf.insert(std::make_pair(label, ReadString(pStream, label)));
			break;

		case TDF_TYPE_BINARY:
			tdf.insert(std::make_pair(label, ReadInteger(pStream, label)));
			break;

		case TDF_TYPE_STRUCT:
			tdf.insert(std::make_pair(label, ReadStruct(pStream, label)));
			break;

		case TDF_TYPE_LIST:
			tdf.insert(std::make_pair(label, ReadList(pStream, label)));
			break;

		case TDF_TYPE_UNION:
			tdf.insert(std::make_pair(label, ReadUnion(pStream, label)));
			break;

		default:
			LOG_ERROR("Unknown Tdf type: %d.", type);
			break;
		}
	}

	std::map<std::string, Tdf*> TdfDecoder::ReadData(Stream* pStream)
	{
		std::map<std::string, Tdf*> tdf_data;

		while (pStream->m_readPos < pStream->m_maxSize)
		{
			ReadTdf(pStream, tdf_data);
			
			uint8_t endOfTdf = 0xFF;
			pStream->ReadByte(&endOfTdf);

			if (endOfTdf != 0)
			{
				break;
			}
		}

		return tdf_data;
	}
}