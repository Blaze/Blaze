#pragma once

namespace Blaze
{
	class TdfBinary : public Tdf
	{
	public:
		int mValue;

		TdfBinary(std::string label, int value);

		int operator=(TdfBinary* pTdf);
		void operator=(int pTdf);
	};
}