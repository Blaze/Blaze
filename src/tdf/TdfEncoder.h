#pragma once

namespace Blaze
{
	class TdfEncoder
	{
	private:
		Stream* mStream;

		void WriteString(const char* pContent);
		void WriteInteger(int pInt);
		void WriteUnion(TdfUnion* pTdf);
		void WriteStruct(TdfStruct* pTdf);
		void WriteList(TdfList* pTdf);
		void WriteMap(TdfMap* pTdf);

	public:
		TdfEncoder(Stream* stream);
		~TdfEncoder();

		void WriteTdf(Tdf* pTdf);
	};
}