#pragma once

namespace Blaze
{
	class TdfList : public Tdf
	{
	public:
		TdfList(std::string label, uint8_t type);

		std::map<int, void*> mList;
		uint8_t mListType;
	};
}