#pragma once

namespace Blaze
{
	enum TdfBaseType : uint8_t
	{
		TDF_TYPE_MIN = 0x0,
		TDF_TYPE_INTEGER = 0x0,
		TDF_TYPE_STRING = 0x1,
		TDF_TYPE_BINARY = 0x2,
		TDF_TYPE_STRUCT = 0x3,
		TDF_TYPE_LIST = 0x4,
		TDF_TYPE_MAP = 0x5,
		TDF_TYPE_UNION = 0x6,
		TDF_TYPE_VARIABLE = 0x7,
		TDF_TYPE_BLAZE_OBJECT_TYPE = 0x8,
		TDF_TYPE_BLAZE_OBJECT_ID = 0x9,
		TDF_TYPE_FLOAT = 0xA,
		TDF_TYPE_TIMEVALUE = 0xB,
		TDF_TYPE_MAX = 0xC
	};

	enum TdfIntType
	{
		TDF_INT_TYPE_UNDEFINED = 0x0,
		TDF_INT_TYPE_U8 = 0x1,
		TDF_INT_TYPE_I8 = 0x2,
		TDF_INT_TYPE_U16 = 0x3,
		TDF_INT_TYPE_I16 = 0x4,
		TDF_INT_TYPE_U32 = 0x5,
		TDF_INT_TYPE_I32 = 0x6,
		TDF_INT_TYPE_U64 = 0x7,
		TDF_INT_TYPE_I64 = 0x8
	};

	class Tdf
	{
	public:
		TdfBaseType mType;
		std::string mLabel;

		Tdf(TdfBaseType type);
	};
}

#include "../tdf/TdfMin.h"
#include "../tdf/TdfInteger.h"
#include "../tdf/TdfString.h"
#include "../tdf/TdfBinary.h"
#include "../tdf/TdfStruct.h"
#include "../tdf/TdfList.h"
#include "../tdf/TdfMap.h"
#include "../tdf/TdfUnion.h"

#include "../tdf/TdfEncoder.h"
#include "../tdf/TdfDecoder.h"