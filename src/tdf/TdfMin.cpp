#include "StdInc.h"

namespace Blaze
{
	TdfMin::TdfMin(std::string label, uint8_t value)
		: Tdf(TDF_TYPE_MIN)
	{
		mLabel = label;
		mValue = value;
	}

	void TdfMin::operator=(uint8_t pTdf)
	{
		mValue = pTdf;
	}

	uint8_t TdfMin::operator=(TdfMin* pTdf)
	{
		return ((TdfMin*)pTdf)->mValue;
	}
}