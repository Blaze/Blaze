#pragma once

namespace Blaze
{
	class TdfUnion : public Tdf
	{
	public:
		int mValue;

		TdfUnion(std::string label, int value);

		int operator=(TdfUnion* pTdf);
		void operator=(int pTdf);

		std::map<std::string, Tdf*> mStruct;
	};
}