#pragma once

namespace Blaze
{
	class TdfDecoder
	{
	public:
		TdfString* ReadString(Stream* pStream, std::string tag);

		int ReadInteger(Stream* pStream);
		TdfInteger* ReadInteger(Stream* pStream, std::string tag);

		TdfMin* ReadMin(Stream* pStream, std::string tag);

		TdfUnion* ReadUnion(Stream* pStream, std::string label);

		TdfStruct* ReadStruct(Stream* pStream, std::string label);
		TdfList* ReadList(Stream* pStream, std::string label);

		// tag reading functions
		std::string TdfDecoder::ReadTag(Stream* pStream);
		std::string TdfDecoder::ReadTag(Stream* pStream, bool* isMin);

		void ReadTdf(Stream* pStream, std::map<std::string, Tdf*>& tdf);
		std::map<std::string, Tdf*> ReadData(Stream* pStream);
	};
}