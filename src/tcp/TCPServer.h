#pragma once

class TCPServer
{
private:
	static TCPServer* m_pInstance;

public:
	TCPListener* m_pListener;

	TCPServer(int port);

	bool Start();

	static void ProcessData(SocketObj from, char* data, int length);
	virtual void HandleData(SocketObj from, char* data, int length) = 0;
};