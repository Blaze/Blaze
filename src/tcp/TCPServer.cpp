#include "StdInc.h"

TCPServer* TCPServer::m_pInstance;

TCPServer::TCPServer(int port)
{
	m_pListener = new TCPListener(port);
	m_pListener->m_ReceivedDataCB = ProcessData;

	m_pInstance = this;
}

bool TCPServer::Start()
{
	return m_pListener->Start();
}

void TCPServer::ProcessData(SocketObj from, char* data, int length)
{
	m_pInstance->HandleData(from, data, length);
}