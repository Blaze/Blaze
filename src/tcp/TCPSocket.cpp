#include "StdInc.h"

int g_socketID = 0;

TCPSocket::TCPSocket()
{
	m_socketID = g_socketID++;
	LOG_DEBUG("Opening TCP/IP socket with ID %i.", m_socketID);

	m_socket = socket(AF_INET, SOCK_STREAM, 0);

	if (!m_socket)
	{
		LOG_ERROR("socket() failed - %i.", WSAGetLastError());
	}
}

TCPSocket::TCPSocket(SOCKET sock)
{
	if (sock == INVALID_SOCKET)
	{
		LOG_ERROR("Cannot use existing socket object that is invalid.");
	}
	else
	{
		m_socketID = g_socketID++;
		m_socket = sock;
	}
}

TCPSocket::~TCPSocket()
{
	LOG_DEBUG("Closing TCP/IP socket ID %i.", g_socketID);

	closesocket(m_socket);
}

bool TCPSocket::isValid()
{
	return m_socket != INVALID_SOCKET;
}

int TCPSocket::bind(const sockaddr* name, int namelen)
{
	return ::bind(m_socket, name, namelen);
}

int TCPSocket::listen(int backlog)
{
	return ::listen(m_socket, backlog);
}

TCPSocket* TCPSocket::accept(sockaddr* addr, int* addrlen)
{
	return new TCPSocket(::accept(m_socket, addr, addrlen));
}

int TCPSocket::connect(sockaddr* name, int namelen)
{
	return ::connect(m_socket, name, namelen);
}

int TCPSocket::recv(char* buf, int len, int flags)
{
	return ::recv(m_socket, buf, len, flags);
}

int TCPSocket::send(const char* buf, int len, int flags)
{
	return ::send(m_socket, buf, len, flags);
}