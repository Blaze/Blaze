#include "StdInc.h"

TCPListener* TCPListener::m_pInstance;

TCPListener::TCPListener(int port)
	: m_listenPort(port)
{
	m_pMutex = new Mutex();
	m_pSocket = new TCPSocket();
	m_pSocketPool = new TCPSocketStack();

	m_numConnections = 0;
	m_totalBytesReceived = 0;

	m_isListening = false;
	m_pInstance = this;
}

TCPListener::~TCPListener()
{
	m_isListening = false;

	delete m_pSocket;
	delete m_pMutex;
	delete m_pSocketPool;
}

bool TCPListener::Start()
{
	if (m_pSocket->isValid())
	{
		sockaddr_in sadr;
		memset(&sadr, 0, sizeof(sockaddr_in));

		// Create sockaddr
		sadr.sin_addr.s_addr = INADDR_ANY;
		sadr.sin_port = htons(m_listenPort);
		sadr.sin_family = AF_INET;

		// Bind
		if (m_pSocket->bind((sockaddr*)&sadr, sizeof(sadr)) != SOCKET_ERROR)
		{
			// Listen
			if (m_pSocket->listen(SOMAXCONN) != SOCKET_ERROR)
			{
				m_isListening = true;

				// Start receiving new connections
				_beginthread(ProcessAccept, 0, NULL);

				LOG_INFO("Listening on port %i.", m_listenPort);
			}
			else
			{
				LOG_ERROR("Couldn't listen on port %i: %d.", m_listenPort, WSAGetLastError());

				return false;
			}
		}
		else
		{
			LOG_ERROR("Couldn't bind socket: %d.", WSAGetLastError());

			return false;
		}

		return true;
	}
	else
	{
		LOG_ERROR("Couldn't create socket.");
	}

	return false;
}

void TCPListener::ProcessAccept(void* pArg)
{
	// Lock the mutex
	m_pInstance->m_pMutex->Lock();

	TCPSocket* pSocket = m_pInstance->m_pSocket;

	sockaddr_in sadr;
	int addrlen = sizeof(sockaddr_in);

	// Accept new connections while the server's listening
	while (m_pInstance->m_isListening)
	{
		memset(&sadr, 0, sizeof(sockaddr_in));

		TCPSocket* pConn = pSocket->accept((sockaddr*)&sadr, &addrlen);

		if (pConn->isValid())
		{
			// Push new connection to our socket pool
			m_pInstance->m_pSocketPool->Push({ sadr, pConn });

			// Increment number of active connections
			InterlockedIncrement(&m_pInstance->m_numConnections);

			LOG_DEBUG("Accepted new connection from %s:%i.", inet_ntoa(sadr.sin_addr), htons(sadr.sin_port));
			
			// Start receiving data from our new connection
			_beginthread(ProcessReceive, 0, NULL);
		}
		else
		{
			LOG_WARN("Received invalid socket connection.");
		}
	}

	LOG_ERROR("No longer listening on port %i.", m_pInstance->m_listenPort);

	// Unlock the mutex when we're done
	m_pInstance->m_pMutex->Unlock();
}

void TCPListener::ProcessReceive(void* pArg)
{
	SocketObj obj = m_pInstance->m_pSocketPool->Pop();

	char buf[131072];

	// Receive data...
	int recvlen = obj.m_pSocket->recv(buf, sizeof(buf), 0);

	// ... while server is listening and everything else needed is correct
	while (m_pInstance->m_isListening && obj.m_pSocket->isValid() && recvlen != SOCKET_ERROR)
	{
		// Process the received data
		if (recvlen > 0)
		{
			buf[recvlen] = '\0';
			m_pInstance->m_totalBytesReceived += recvlen;

			if (m_pInstance->m_ReceivedDataCB)
			{
				m_pInstance->m_ReceivedDataCB(obj, buf, recvlen);
			}
		}
		else if (!recvlen)
		{
			break;
		}

		recvlen = obj.m_pSocket->recv(buf, sizeof(buf), 0);
	}

	LOG_INFO("Client disconnected (%s:%i).", inet_ntoa(obj.m_sadr.sin_addr), htons(obj.m_sadr.sin_port));

	// Client disconnected, decrement number of active connections
	InterlockedDecrement(&m_pInstance->m_numConnections);
}