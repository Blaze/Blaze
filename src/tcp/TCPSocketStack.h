#pragma once

struct SocketObj
{
	sockaddr_in m_sadr;
	TCPSocket *m_pSocket;
};

class TCPSocketStack : public Stack<SocketObj>
{
public:
	SocketObj Pop();
	void Push(SocketObj pObj);
};