#pragma once

typedef void(*ReceivedDataCB_t)(SocketObj obj, char* data, int length);

class TCPListener
{
private:
	int m_listenPort;

	Mutex *m_pMutex;
	TCPSocket *m_pSocket;
	TCPSocketStack *m_pSocketPool;

	static TCPListener *m_pInstance;

public:
	long m_numConnections;
	unsigned int m_totalBytesReceived;
	bool m_isListening;

	ReceivedDataCB_t m_ReceivedDataCB;

	TCPListener(int port);
	~TCPListener();

	bool Start();

	static void ProcessAccept(void* pArg);
	static void ProcessReceive(void* pArg);
};