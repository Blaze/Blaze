#pragma once

namespace Blaze
{
	class FireFrame
	{
	public:
		uint16_t mSize;
		int mComponentID;
		int mCommandID;
		int mErrorCode;
		unsigned int mMsgType;
		int mMsgNum;
		char* mPayload;
		int mLength;

		bool ReadData(char* data, int length);
	};
}