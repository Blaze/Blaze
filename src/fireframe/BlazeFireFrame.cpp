#include "StdInc.h"

namespace Blaze
{
	bool FireFrame::ReadData(char* data, int length)
	{
		if (length < 0xE)
		{
			LOG_ERROR("Packet is too small to be read.");
		}

		mSize = HIBYTE(*(short*)data);
		//mSize = (uint8_t)data[1] | ((uint8_t)*data << 8);
		//
		//if (data[9] & 0x10)
		//{
		//	mSize |= ((uint8_t)data[13] | ((uint8_t)data[12] << 8)) << 16;
		//}

		mComponentID = data[3] | (data[2] << 8);
		mCommandID = data[5] | (data[4] << 8);
		mErrorCode = data[7] | (data[6] << 8);
		mMsgType = (unsigned int)data[8] >> 4;
		mMsgNum = data[11] | ((data[10] | ((data[9] & 0xF) << 8)) << 8);

		// check for packets not containing any Tdf data
		//if (!mSize)
		//{
		//	mLength = 0;
		//}
		//else
		{
			mLength = length - 14;
			mPayload = (char*)malloc(mSize);

			memcpy(mPayload, &data[14], mSize);
		}

		return true;
	}
}