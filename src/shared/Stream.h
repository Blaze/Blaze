#pragma once

class Stream
{
public:
	char* m_data;
	int m_maxSize;
	int m_writePos;
	int m_readPos;

	Stream(char* data, int size);
	~Stream();

	bool Read(void* data, int count);
	bool Write(void* data, int count);

	bool ReadTag(std::string& tag);
	bool WriteTag(std::string tag);

	bool ReadByte(uint8_t* data);
	bool WriteByte(uint8_t data);

	bool ReadInt32(int* data);
	bool WriteInt32(int data);

	bool ReadString(std::string& data, uint8_t length);
	bool WriteString(const char* data, int length);

	bool WriteInt16(short data);
};