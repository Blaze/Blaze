#include "StdInc.h"

bool SetWindowSize(int width, int height)
{
	_COORD coord;
	coord.X = width;
	coord.Y = height;

	_SMALL_RECT rect;
	rect.Top = 0;
	rect.Left = 0;
	rect.Bottom = height - 1;
	rect.Right = width - 1;

	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	if (!SetConsoleScreenBufferSize(handle, coord) || !SetConsoleWindowInfo(handle, TRUE, &rect))
	{
		LOG_ERROR("Couldn't set window size.");

		return false;
	}

	return true;
}

bool InitializeWinsock()
{
	WSADATA data;
	int result;

	if ((result = WSAStartup(MAKEWORD(2, 2), &data)))
	{
		LOG_ERROR("Couldn't initialize Winsock (0x%x).", result);

		return false;
	}

	return true;
}

bool StartServers()
{
	//Blaze::HubServer* pHubServer = new Blaze::HubServer();
	Blaze::Server* pServer = new Blaze::Server();

	if (/* !pHubServer->Start() || */!pServer->Start())
	{
		LOG_ERROR("Couldn't start one of the main servers.");

		return false;
	}

	return true;
}

ServerMode g_serverMode;

int main(int argc, char** argv)
{
#ifdef WIN32
	// set console window size
	SetWindowSize(130, 50);
#endif

#ifdef DEBUG
	if (!Log::Init(LogLevel_All))
#else
	if (!Log::Init(LogLevel_All))
#endif
	{
		return 1;
	}

	// set server mode
	g_serverMode = Config::GetServerMode();

	//LOG_INFO("Starting in '%s' mode.", instance.c_str());

	// initialize Winsock and start servers
	if (!InitializeWinsock() || !StartServers())
	{
		return 1;
	}

	// loop
	for (;;)
	{

	}

	return 0;
}