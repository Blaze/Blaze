#include "StdInc.h"

bool Utilities::FileExists(std::string fileName)
{
	if (boost::filesystem::exists(fileName))
	{
		return true;
	}

	return false;
}

std::string Utilities::GetAbsoluteServerPath()
{
	static std::string serverPath;

	if (!serverPath.size())
	{
		char modulePath[512];
		GetModuleFileNameA(GetModuleHandleA(nullptr), modulePath, sizeof(modulePath) / sizeof(wchar_t));

		char* dirPtr = strrchr(modulePath, L'\\');

		// we do not check if dirPtr happens to be 0, as any valid absolute Win32 file path contains at least one backslash
		dirPtr[1] = '\0';

		serverPath = modulePath;
	}

	return serverPath;
}

std::string Utilities::MakeRelativeServerPath(std::string targetPath)
{
	return GetAbsoluteServerPath() + targetPath;
}

bool Utilities::has_suffix(const std::string &str, const std::string &suffix)
{
	return str.size() >= suffix.size() &&
		str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

unsigned int Utilities::GetUnixTime()
{
	return std::time(0);
}