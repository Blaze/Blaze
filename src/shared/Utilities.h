#pragma once

class Utilities
{
public:
	static bool FileExists(std::string fileName);

	// CitizenIV - NTAuthority
	static std::string GetAbsoluteServerPath();
	static std::string MakeRelativeServerPath(std::string targetPath);

	// http://stackoverflow.com/questions/20446201/how-to-check-if-string-ends-with-txt
	static bool has_suffix(const std::string &str, const std::string &suffix);

	static unsigned int GetUnixTime();
};