#include "StdInc.h"

Config::Config()
{
	
}

db_info Config::GetDatabaseInfo()
{
	db_info dbInfo = db_info();

	YAML::Node config = YAML::LoadFile(Utilities::MakeRelativeServerPath(CONFIG_FILENAME));

	dbInfo.host = config["host"].as<std::string>().c_str();
	dbInfo.username = config["username"].as<std::string>().c_str();
	dbInfo.password = config["password"].as<std::string>().c_str();
	dbInfo.database = config["database"].as<std::string>().c_str();

	return dbInfo;
}

ServerMode Config::GetServerMode()
{
	YAML::Node config = YAML::LoadFile(Utilities::MakeRelativeServerPath(CONFIG_FILENAME));

	std::string configMode = config["mode"].as<std::string>();

	ServerMode mode = BATTLEFIELD_3_PC; // default mode

	if (configMode != "")
	{
		if (configMode == "battlefield-4-pc")
		{
			mode = BATTLEFIELD_4_PC;
		}
	}

	return mode;
}