#pragma once

#define LOG_FILENAME "BlazeServer.log"
#define CONFIG_FILENAME "config.yaml"

#define DECODER_DEBUG
#define DUMPTY_DUMP

#define INSTANCE "battlefield-3-pc" // battlefield-4-pc

#define TELEMETRY_SERVER_ADDRESS "10.0.0.1"
#define TELEMETRY_SERVER_PORT 8999

#define TICKER_SERVER_ADDRESS "10.0.0.1"
#define TICKER_SERVER_PORT 8999

#include "buildnumber.h"

#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN

// Windows
#include <WinSock2.h>
#include <Windows.h>
#include <process.h>

// Standard headers
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <stack>
#include <vector>
#include <map>
#include <string>
#include <algorithm>

#include <iostream>
#include <sstream>

// boost
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/any.hpp>
#include <boost/lexical_cast.hpp>

// yaml-cpp
#include <yaml.h>

// mysql
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/sqlstring.h>
#include <cppconn/prepared_statement.h>

enum ServerMode
{
	BATTLEFIELD_3_PC = 0,
	BATTLEFIELD_4_PC = 1
};

extern ServerMode g_serverMode;

// common
#include "Log.h"
#include "Stream.h"
#include "Config.h"
#include "Utilities.h"

#include "Mutex.h"
#include "Stack.h"

// database
#include "../database/Database.h"

// tcp
#include "../tcp/TCPSocket.h"
#include "../tcp/TCPSocketStack.h"
#include "../tcp/TCPListener.h"
#include "../tcp/TCPServer.h"

// blaze
#include "../blaze/Blaze.h"
#include "../blaze/BlazeServerBase.h"

#include "../server/BlazeHub.h"
#include "../server/BlazeServer.h"