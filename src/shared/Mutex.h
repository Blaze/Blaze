#pragma once

class Mutex
{
private:
	HANDLE m_handle;

public:
	Mutex();
	~Mutex();

	void Lock();
	void Unlock();
};