#pragma once

template <typename T>
class Stack
{
protected:
	std::stack<T> m_stack;

public:
	T Pop()
	{
		T obj = m_stack.top();
		m_stack.pop();

		return obj;
	}

	void Push(T obj)
	{
		m_stack.push(obj);
	}
};