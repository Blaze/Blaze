#pragma once

struct db_info
{
	std::string host;
	std::string username;
	std::string password;
	std::string database;
};

class Config
{
public:
	Config();
	static db_info GetDatabaseInfo();
	static ServerMode GetServerMode();
};