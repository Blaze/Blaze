#pragma once

// Credits to Almamu

enum LogLevel
{
	LogLevel_None = 0,
	LogLevel_Warning = 1,
	LogLevel_Error = 2,
	LogLevel_Debug = 4,
	LogLevel_Info = 8,
	LogLevel_Trace = 16,
	LogLevel_All = 31,
};

class Log
{
private:
	static LogLevel logLevel;
	static FILE* fileStream;
	static bool enabled;
	static uint32_t queue;
	
public:
	static bool Init(LogLevel logLevel);
	static void Print(LogLevel type, const char* identifier, const char* svc, const char* msg);
	static void Debug(const char* svc, const char* msg, ...);
	static void Error(const char* svc, const char* msg, ...);
	static void Trace(const char* svc, const char* msg, ...);
	static void Info(const char* svc, const char* msg, ...);
	static void Warning(const char* svc, const char* msg, ...);
	static void Stop();
};

#define LOG_DEBUG(fmt, ...) Log::Debug(__FUNCTION__, fmt, __VA_ARGS__)
#define LOG_ERROR(fmt, ...) Log::Error(__FUNCTION__, fmt, __VA_ARGS__)
#define LOG_TRACE(fmt, ...) Log::Trace(__FUNCTION__, fmt, __VA_ARGS__)
#define LOG_INFO(fmt, ...) Log::Info(__FUNCTION__, fmt, __VA_ARGS__)
#define LOG_WARN(fmt, ...) Log::Warning(__FUNCTION__, fmt, __VA_ARGS__)