#include "StdInc.h"

Mutex::Mutex()
{
	m_handle = CreateMutexA(0, FALSE, 0);
}

Mutex::~Mutex()
{
	ReleaseMutex(m_handle);
	CloseHandle(m_handle);
}

void Mutex::Lock()
{
	WaitForSingleObject(m_handle, -1);
}

void Mutex::Unlock()
{
	ReleaseMutex(m_handle);
}