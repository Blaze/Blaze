#include "StdInc.h"
#include <time.h>

// Credits to Almamu

bool Log::Init(LogLevel logLevel)
{
	fileStream = fopen(LOG_FILENAME, "ab+");

	if (fileStream == NULL)
	{
		return false;
	}

	enabled = true;

	SetConsoleTitleA("BlazeServer");

	Log::logLevel = logLevel;

	return true;
}

void Log::Print(LogLevel type, const char* identifier, const char* svc, const char* msg)
{
	if (enabled == false)
	{
		return;
	}

	if ((type & logLevel) == 0)
	{
		return;
	}

	while (queue > 0)
	{
		Sleep(1);
	}

	queue++;

	char date[9];
	_strdate_s(date);
	
	date[2] = '-';
	date[5] = '-';

	printf("%s ", date);
	fprintf(fileStream, "%s ", date);

	char time[9];
	_strtime_s(time);

	printf("%s - ", time);
	fprintf(fileStream, "%s - ", time);

	printf("%s: ", svc);
	fprintf(fileStream, ":%s: ", svc);

	printf("%s: ", identifier);
	fprintf(fileStream, "%s: ", identifier);

	printf("%s\r\n", msg);
	fprintf(fileStream, "%s\r\n", msg);

	queue--;
}

void Log::Debug(const char* svc, const char* msg, ...)
{
	char logline[1024];

	va_list argumentList;
	va_start(argumentList, msg);

	vsnprintf(logline, 1024, msg, argumentList);

	va_end(argumentList);

	Print(LogLevel_Debug, "DEBUG", svc, logline);
}

void Log::Error(const char* svc, const char* msg, ...)
{
	char logline[1024];

	va_list argumentList;
	va_start(argumentList, msg);

	vsnprintf(logline, 1024, msg, argumentList);

	va_end(argumentList);

	Print(LogLevel_Error, "ERROR", svc, logline);
}

void Log::Info(const char* svc, const char* msg, ...)
{
	char logline[1024];

	va_list argumentList;
	va_start(argumentList, msg);

	vsnprintf(logline, 1024, msg, argumentList);

	va_end(argumentList);

	Print(LogLevel_Info, "INFO", svc, logline);
}

void Log::Trace(const char* svc, const char* msg, ...)
{
	char logline[1024];

	va_list argumentList;
	va_start(argumentList, msg);

	vsnprintf(logline, 1024, msg, argumentList);

	va_end(argumentList);

	Print(LogLevel_Trace, "TRACE", svc, logline);
}

void Log::Warning(const char* svc, const char* msg, ...)
{
	char logline[1024];

	va_list argumentList;
	va_start(argumentList, msg);

	vsnprintf(logline, 1024, msg, argumentList);

	va_end(argumentList);

	Print(LogLevel_Warning, "WARN", svc, logline);
}

void Log::Stop()
{
	if (enabled == false)
	{
		return;
	}

	while (queue > 0);

	fclose(fileStream);

	enabled = false;
}

LogLevel Log::logLevel = LogLevel_None;
FILE* Log::fileStream = NULL;
bool Log::enabled = false;
uint32_t Log::queue = 0;