#include "StdInc.h"

Stream::Stream(char* data, int size)
	: m_data(data), m_maxSize(size), m_writePos(0), m_readPos(0)
{

}

Stream::~Stream()
{
	//delete m_data;
	//m_maxSize = 0;
	//m_writePos = 0;
	//m_readPos = 0;
}

bool Stream::Read(void* data, int count)
{
	if ((count + m_readPos) < m_maxSize)
	{
		memcpy(data, &m_data[m_readPos], count);
		m_readPos += count;

		return true;
	}

	return false;
}

bool Stream::Write(void* data, int count)
{
	if ((count + m_writePos) < m_maxSize)
	{
		memcpy(&m_data[m_writePos], data, count);
		m_writePos += count;

		return true;
	}

	return false;
}

bool Stream::WriteTag(std::string tag)
{
	int wTag = 0;
	int labelSize = tag.size();

	switch (labelSize)
	{
	case 2:
		for (int i = 0; i < MAX_LABEL_SIZE - 2; i++)
		{
			wTag |= (0x20 | (tag[i] & 0x1F)) << (3 - i) * 6;
		}

		break;

	case 3:
		for (int i = 0; i < MAX_LABEL_SIZE - 1; i++)
		{
			wTag |= (0x20 | (tag[i] & 0x1F)) << (3 - i) * 6;
		}

		break;

	default:
		for (int i = 0; i < MAX_LABEL_SIZE; i++)
		{
			wTag |= (0x20 | (tag[i] & 0x1F)) << (3 - i) * 6;
		}

		break;
	}

	wTag = _byteswap_ulong(wTag) >> 8;

	return Write(&wTag, 3);
}

// read tag to label
bool Stream::ReadTag(std::string& label)
{
	char eTag[3];
	
	if (Read(eTag, sizeof(eTag)))
	{
		int iTag = _byteswap_ulong(*(int*)eTag) >> 8;
		
		for (int i = 0; i < sizeof(int); i++)
		{
			int j = (iTag >> (3 - i) * 6) & 0x3F;

			if (j > 0)
			{
				label += (uint8_t)(0x40 | (j & 0x1F));
			}
		}

		return true;
	}

	return false;
}

bool Stream::ReadByte(uint8_t* data)
{
	return Read(data, sizeof(uint8_t));
}

bool Stream::ReadInt32(int* data)
{
	return Read(data, sizeof(int));
}

bool Stream::ReadString(std::string& data, uint8_t length)
{
	uint8_t b = 0xFF;
	std::string str = "";

	for (int i = 0; i < length; i++)
	{
		if (!ReadByte(&b))
		{
			return false;
		}

		str += b;
	}

	data = str;

	return true;
}

bool Stream::WriteByte(uint8_t data)
{
	return Write(&data, sizeof(uint8_t));
}

bool Stream::WriteInt32(int data)
{
	return Write(&data, sizeof(int));
}

bool Stream::WriteString(const char* data, int length)
{
	if (!length) length = strlen(data);

	int len = length + 1;

	for (int i = 0; i < len - 1; i++)
	{
		if (!WriteByte(data[i]))
		{
			return false;
		}
	}

	return true;
}

bool Stream::WriteInt16(short data)
{
	return Write(&data, sizeof(short));
}