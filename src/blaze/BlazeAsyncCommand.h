#pragma once

namespace Blaze
{
	class AsyncCommand
	{
	public:
		BlazeComponent mComponentID;
		BlazeCommand mCommandID;

		AsyncCommand(BlazeComponent componentID, BlazeCommand commandID);

		virtual Reply* CreateReply(SocketObj source) = 0;
	};
}