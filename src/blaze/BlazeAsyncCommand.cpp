#include "StdInc.h"

namespace Blaze
{
	AsyncCommand::AsyncCommand(BlazeComponent componentID, BlazeCommand commandID)
		: mComponentID(componentID), mCommandID(commandID)
	{

	}
}