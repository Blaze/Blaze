#include "StdInc.h"

namespace Blaze
{
	Reply::Reply(uint16_t componentID, uint16_t commandID, uint16_t errorCode, ReplyType messageType, uint16_t messageID)
		: mComponentID(componentID), mCommandID(commandID), mErrorCode(errorCode), mMessageType(messageType), mMessageID(messageID)
	{
		memset(mPayloadData, 0, sizeof(mPayloadData));
		memset(mPacketData, 0, sizeof(mPacketData));

		mPayloadStream = new Stream(mPayloadData, sizeof(mPayloadData));
		mPacketStream = new Stream(mPacketData, sizeof(mPacketData));

		mTdfStream = new TdfEncoder(mPayloadStream);
	}

	Reply::~Reply()
	{
		
	}

	void Reply::WriteTdf(Tdf* pTdf)
	{
		mTdfStream->WriteTdf(pTdf);
	}

	bool Reply::Send(SocketObj to)
	{
		// update last component and command id
		//std::map<sockaddr_in, BlazeClient*>::iterator clientIterator = clientList.find(to.m_sadr);
		
		//if (clientIterator != clientList.end())
		{
		//	clientIterator->second->lastComponentID = mComponentID;
		//	clientIterator->second->lastCommandID = mCommandID;
		}

		// write payload length
		mPacketStream->WriteByte((mPayloadStream->m_writePos & 0xFFFF) >> 8);
		mPacketStream->WriteByte(mPayloadStream->m_writePos);

		// write component ID
		mPacketStream->WriteByte(HIBYTE(mComponentID));
		mPacketStream->WriteByte(mComponentID);

		// write command ID
		mPacketStream->WriteByte(HIBYTE(mCommandID));
		mPacketStream->WriteByte(mCommandID);

		// write error code
		mPacketStream->WriteByte(HIBYTE(mErrorCode));
		mPacketStream->WriteByte(mErrorCode);

		// write message type
		mPacketStream->WriteByte(mMessageType >> 8);
		mPacketStream->WriteByte(mMessageType & 0xFF);

		// write message ID
		mPacketStream->WriteByte(_byteswap_ushort(mMessageID));
		mPacketStream->WriteByte(mMessageID);

		// write payload
		mPacketStream->Write(mPayloadStream->m_data, mPayloadStream->m_writePos);

		// dump output packet
#ifdef DUMPTY_DUMP
		//if (!_stricmp(getenv("computername"), "w3d"))
		{
			char safeFilename[MAX_PATH];
			sprintf(safeFilename, "C:\\dev\\dump\\output2-component-%i-command-%i.bin", mComponentID, mCommandID);

			FILE* fp = fopen(safeFilename, "wb");
			fwrite(mPacketStream->m_data, mPacketStream->m_writePos, 1, fp);
			fclose(fp);
		}
#endif

		struct packet
		{
			const char* data;
			int len;
		};

		std::map<sockaddr_in, packet, cmpBySocket> outputPacketList;

		//return true;

		return to.m_pSocket->send(mPacketStream->m_data, mPacketStream->m_writePos, 0) != SOCKET_ERROR;
	}
}