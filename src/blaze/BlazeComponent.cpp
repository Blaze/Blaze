#include "StdInc.h"

namespace Blaze
{
	Component::Component(BlazeComponent componentID)
		: mComponentID(componentID)
	{

	}

	void Component::AddCommand(Command* pCommand)
	{
		m_commands.push_back(pCommand);
	}

	bool Component::HandleData(SocketObj source, FireFrame* pFireFrame)
	{
		bool bHandled = false;

		for (mCmdItr itr = m_commands.begin(); itr != m_commands.end(); itr++)
		{
			Command* pCommand = *itr;

			if (pCommand->mCommandID == pFireFrame->mCommandID)
			{
				pCommand->mComponent = this;
				bHandled = true;

				if (pFireFrame->mCommandID == UPDATENETWORKINFO_COMMAND)
				{
					// read data so the command can use it
					Stream* pStream = new Stream(pFireFrame->mPayload, pFireFrame->mLength);

					TdfDecoder* pReader = new TdfDecoder();
					pCommand->ReadParsedData(pReader->ReadData(pStream));

					char safeFilename[MAX_PATH];
					sprintf(safeFilename, "C:\\dev\\dump\\input-payload-dump.bin");

					FILE* fp = fopen(safeFilename, "wb");
					fwrite(pFireFrame->mPayload, strlen(pFireFrame->mPayload), 1, fp);
					fclose(fp);
				}

				Reply reply = pCommand->HandleData(pFireFrame, source);
				reply.Send(source);

				bHandled = true;
			}
		}

		if (!bHandled)
		{
			LOG_ERROR("Received unhandled command (%i) (mComponentID = %i).", pFireFrame->mCommandID, pFireFrame->mComponentID);
		}

		return bHandled;
	}
}