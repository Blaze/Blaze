#pragma once

#define MAX_LABEL_SIZE 4

enum MessageType
{
	MESSAGE = 0x0,
	REPLY = 0x1,
	NOTIFICATION = 0x2,
	ERROR_REPLY = 0x3
};

enum NatType
{
	NAT_TYPE_OPEN = 0x0,
	NAT_TYPE_MODERATE = 0x1,
	NAT_TYPE_STRICT_SEQUENTIAL = 0x2,
	NAT_TYPE_STRICT = 0x3,
	NAT_TYPE_UNKNOWN = 0x4
};

enum TelemetryOpt
{
	TELEMETRY_OPT_OUT = 0x0,
	TELEMETRY_OPT_IN = 0x1
};

enum BlazeComponent
{
	REDIRECTOR_COMPONENT = 0x0005,
	UTIL_COMPONENT = 0x0009,
	AUTHENTICATION_COMPONENT = 0x0001,
	GAMEMANAGER_COMPONENT = 0x0004,

	// async
	USERSESSIONS_COMPONENT = 0x7802,
};

enum BlazeCommand
{
	// RedirectorComponent
	GETSERVERINSTANCE_COMMAND = 0x0001,

	// UtilComponent
	PING_COMMAND = 0x0002,
	PREAUTH_COMMAND = 0x0007,
	POSTAUTH_COMMAND = 0x0008,
	FETCHCLIENTCONFIG_COMMAND = 0x0001,
	GETTELEMETRYSERVER_COMMAND = 0x0005,

	CREATEPERSONA_COMMAND = 0x0050,

	LOGIN_COMMAND = 0x0028,
	LOGINPERSONA_COMMAND = 0x006E,

	// UserSessionsComponent
	UPDATENETWORKINFO_COMMAND = 0x0014,
	USERADDED_COMMAND = 0x0002,
	USERUPDATED_COMMAND = 0x0005,
	USERSESSIONEXTENDEDDATAUPDATE_COMMAND = 0x0001
};

enum ClientType
{
	CLIENT_TYPE_GAMEPLAY_USER = 0x0,
	CLIENT_TYPE_HTTP_USER = 0x1,
	CLIENT_TYPE_DEDICATED_SERVER = 0x2,
	CLIENT_TYPE_TOOLS = 0x3,
	CLIENT_TYPE_INVALID = 0x4
};

struct BlazeClient
{
	//bool dedicated = false;
	/*
	uint8_t userID; // use connection id, both client and dedicated? 
	uint8_t gameID; // only if dedicated?
	uint8_t playerID; // only if client (game)?
	*/

	ClientType clientType;

	uint16_t lastComponentID;
	uint16_t lastCommandID;

	unsigned long externalIP;
	unsigned short externalPort;

	unsigned long internalIP;
	unsigned short internalPort;
};

struct cmpBySocket
{
	bool operator()(const sockaddr_in& key1, const sockaddr_in& key2) const
	{
		return key1.sin_port == key2.sin_port && key1.sin_addr.s_addr == key2.sin_addr.s_addr;
	}
};

extern std::map<sockaddr_in, BlazeClient*, cmpBySocket> clientList;

#include "../fireframe/BlazeFireFrame.h"
#include "../tdf/Tdf.h"
#include "BlazeReply.h"

// commands
#include "BlazeCommand.h"
#include "../commands/BlazeGetServerInstanceCommand.h"
#include "../commands/BlazePreAuthCommand.h"
#include "../commands/BlazePingCommand.h"
#include "../commands/BlazeLoginCommand.h"
#include "../commands/BlazeCreatePersonaCommand.h"
#include "../commands/BlazeLoginPersonaCommand.h"
#include "../commands/BlazePostAuthCommand.h"
#include "../commands/BlazeFetchClientConfigCommand.h"
#include "../commands/BlazeUpdateNetworkInfoCommand.h"
#include "../commands/BlazeGetTelemetryServerCommand.h"

// components
#include "BlazeComponent.h"
#include "../components/BlazeRedirectorComponent.h"
#include "../components/BlazeUtilComponent.h"
#include "../components/BlazeAuthenticationComponent.h"
#include "../components/BlazeUserSessionsComponent.h"

// async commands
#include "BlazeAsyncCommand.h"
#include "../commands/async/BlazeUserAddedCommand.h"
#include "../commands/async/BlazeUserUpdatedCommand.h"
#include "../commands/async/BlazeUserSessionExtendedDataUpdateCommand.h"