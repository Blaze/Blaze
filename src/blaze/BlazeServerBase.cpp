#include "StdInc.h"

namespace Blaze
{
	ServerBase::ServerBase(int port)
		: TCPServer(port)
	{
	}

	void ServerBase::AddComponent(Blaze::Component* pComponent)
	{
		m_components.push_back(pComponent);
	}

	void ServerBase::HandleData(SocketObj from, char* data, int length)
	{
		FireFrame* pFireFrame = new FireFrame();
		pFireFrame->ReadData(data, length);

		// dump input packet
#ifdef DUMPTY_DUMP
		if (!_stricmp(getenv("computername"), "w3d"))
		{
			char safeFilename[MAX_PATH];
			sprintf(safeFilename, "C:\\dev\\dump\\input-component-%i-command-%i.bin", pFireFrame->mComponentID, pFireFrame->mCommandID);

			FILE* fp = fopen(safeFilename, "wb");
			fwrite(data, length, 1, fp);
			fclose(fp);
		}
#endif

		typedef std::vector<Component*>::iterator mCompItr;

		bool bHandled = false;

		for (mCompItr itr = m_components.begin(); itr != m_components.end(); itr++)
		{
			Component* pComponent = *itr;

			if (pComponent && pComponent->mComponentID == pFireFrame->mComponentID)
			{
				bHandled = true;

				pComponent->HandleData(from, pFireFrame);
			}
		}

		if (!bHandled)
		{
			LOG_ERROR("Received unhandled component request (mComponentID: %i)", pFireFrame->mComponentID);
		}
	}
}