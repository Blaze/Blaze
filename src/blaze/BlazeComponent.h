#pragma once

struct SocketObj;

namespace Blaze
{
	class Component
	{
	protected:
		std::vector<Command*> m_commands;
		void AddCommand(Command* pCommand);

		// used by HandleData
		typedef std::vector<Command*>::iterator mCmdItr;

	public:
		BlazeComponent mComponentID;

		Component(BlazeComponent componentID);
		bool HandleData(SocketObj source, FireFrame* pFireFrame);
	};
}