#pragma once

namespace Blaze
{
	class ServerBase : public TCPServer
	{
	protected:
		std::vector<Component*> m_components;

	public:
		ServerBase(int port);

		void AddComponent(Component* pComponent);
		void HandleData(SocketObj from, char* data, int length);
	};
}