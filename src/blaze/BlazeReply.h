#pragma once

namespace Blaze
{
	enum ReplyType : unsigned int
	{
		REPLY_NORMAL = 0x1000,
		REPLY_ASYNC = 0x2000,
		REPLY_ERROR = 0x3000
	};

	class Reply
	{
	private:
		char mPayloadData[131072];
		char mPacketData[131072];
		Stream* mPayloadStream;
		Stream* mPacketStream;

	public:
		uint16_t mComponentID;
		uint16_t mCommandID;
		uint16_t mErrorCode;
		ReplyType mMessageType;
		uint16_t mMessageID;
		char* mPayload;
		int mLength;

		Reply(uint16_t componentId, uint16_t command, uint16_t errorCode, ReplyType messageType, uint16_t messageID);
		~Reply();

		TdfEncoder* mTdfStream;
		
		void WriteTdf(Tdf* pTdf);

		Stream* GetStream();
		bool Send(SocketObj to);
	};
}