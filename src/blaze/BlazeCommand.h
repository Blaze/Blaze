#pragma once

namespace Blaze
{
	class Component;

	class Command
	{
	public:
		BlazeCommand mCommandID;
		Component* mComponent;

		Command(BlazeCommand commandID);

		virtual void ReadParsedData(std::map<std::string, Tdf*> parsedData) = 0;
		virtual Reply HandleData(FireFrame* pFireFrame, SocketObj source) = 0;
	};
}