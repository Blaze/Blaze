#include "StdInc.h"

namespace Blaze
{
	GetServerInstanceCommand::GetServerInstanceCommand()
		: Command(GETSERVERINSTANCE_COMMAND)
	{
		
	}

	void GetServerInstanceCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{

	}

	Reply GetServerInstanceCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		TdfStruct* valu = new TdfStruct("VALU");
		valu->mStruct["HOST"] = new TdfString("HOST", "127.0.0.1");
		valu->mStruct["IP"] = new TdfInteger("IP", 0);
		valu->mStruct["PORT"] = new TdfInteger("PORT", 10981);

		reply.WriteTdf(new TdfUnion("ADDR", 0));
		reply.WriteTdf(valu);
		reply.WriteTdf(new TdfInteger("SECU", 0));
		reply.WriteTdf(new TdfInteger("XDNS", 0));

		LOG_DEBUG("Sending reply to GetServerInstanceCommand request.");

		return reply;
	}
}