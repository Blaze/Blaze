#pragma once

namespace Blaze
{
	class GetServerInstanceCommand : public Command
	{
	public:
		GetServerInstanceCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}