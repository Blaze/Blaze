#pragma once

namespace Blaze
{
	class GetTelemetryServerCommand : public Command
	{
	public:
		GetTelemetryServerCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}