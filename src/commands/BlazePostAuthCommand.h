#pragma once

namespace Blaze
{
	class PostAuthCommand : public Command
	{
	public:
		PostAuthCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}