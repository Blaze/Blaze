#pragma once

namespace Blaze
{
	class CreatePersonaCommand : public Command
	{
	private:
		std::string mPersonaName;

	public:
		CreatePersonaCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}