#pragma once

namespace Blaze
{
	class FetchClientConfigCommand : public Command
	{
	public:
		FetchClientConfigCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}