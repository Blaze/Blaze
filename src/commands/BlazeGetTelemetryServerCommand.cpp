#include "StdInc.h"

namespace Blaze
{
	GetTelemetryServerCommand::GetTelemetryServerCommand()
		: Command(GETTELEMETRYSERVER_COMMAND)
	{
		
	}

	void GetTelemetryServerCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{

	}

	Reply GetTelemetryServerCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		UserSessionExtendedDataUpdateCommand* userSessionExtendedDataUpdateCommand = new UserSessionExtendedDataUpdateCommand();
		userSessionExtendedDataUpdateCommand->CreateReply(source)->Send(source);

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		reply.WriteTdf(new TdfString("ADRS", TELEMETRY_SERVER_ADDRESS));
		reply.WriteTdf(new TdfInteger("ANON", 0));

		// telemetryDisable
		reply.WriteTdf(new TdfString("DISA", "AD,AF,AG,AI,AL,AM,AN,AO,AQ,AR,AS,AW,AX,AZ,BA,BB,BD,BF,BH,BI,BJ,BM,BN,BO,BR,BS,BT,BV,BW,BY,BZ,CC,CD,CF,CG,CI,CK,CL,CM,CN,CO,CR,CU,CV,CX,DJ,DM,DO,DZ,EC,EG,EH,ER,ET,FJ,FK,FM,FO,GA,GD,GE,GF,GG,GH,GI,GL,GM,GN,GP,GQ,GS,GT,GU,GW,GY,HM,HN,HT,ID,IL,IM,IN,IO,IQ,IR,IS,JE,JM,JO,KE,KG,KH,KI,KM,KN,KP,KR,KW,KY,KZ,LA,LB,LC,LI,LK,LR,LS,LY,MA,MC,MD,ME,MG,MH,ML,MM,MN,MO,MP,MQ,MR,MS,MU,MV,MW,MY,MZ,NA,NC,NE,NF,NG,NI,NP,NR,NU,OM,PA,PE,PF,PG,PH,PK,PM,PN,PS,PW,PY,QA,RE,RS,RW,SA,SB,SC,SD,SG,SH,SJ,SL,SM,SN,SO,SR,ST,SV,SY,SZ,TC,TD,TF,TG,TH,TJ,TK,TL,TM,TN,TO,TT,TV,TZ,UA,UG,UM,UY,UZ,VA,VC,VE,VG,VN,VU,WF,WS,YE,YT,ZM,ZW,ZZ"));
		reply.WriteTdf(new TdfString("FILT", ""));
		reply.WriteTdf(new TdfInteger("LOC", 1701729619));

		// telemetryNoToggleOk
		reply.WriteTdf(new TdfString("NOOK", "US,CA,MX"));
		reply.WriteTdf(new TdfInteger("PORT", TELEMETRY_SERVER_PORT));

		reply.WriteTdf(new TdfInteger("SDLY", 15000));
		reply.WriteTdf(new TdfString("SESS", "telemetry_session"));
		reply.WriteTdf(new TdfString("SKEY", "telemetry_key"));
		reply.WriteTdf(new TdfInteger("SPCT", 75));
		reply.WriteTdf(new TdfString("STIM", "Default"));

		LOG_DEBUG("Sending reply to getTelemetryServer command.");

		return reply;
	}
}