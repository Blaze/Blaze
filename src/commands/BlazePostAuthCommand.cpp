#include "StdInc.h"

namespace Blaze
{
	PostAuthCommand::PostAuthCommand()
		: Command(POSTAUTH_COMMAND)
	{
		
	}

	void PostAuthCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{

	}

	Reply PostAuthCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		UserAddedCommand* userAddedCommand = new UserAddedCommand();
		userAddedCommand->CreateReply(source)->Send(source);

		UserUpdatedCommand* userUpdatedCommand = new UserUpdatedCommand();
		userUpdatedCommand->CreateReply(source)->Send(source);

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		int userID = 2900;

		TdfStruct* pss = new TdfStruct("PSS");
		pss->mStruct["ADRS"] = new TdfString("ADRS", "10.0.0.1");
		//pss->mStruct["CSIG"] = new TdfBinary("CSIG", 0);
		//pss->mStruct["OIDS"] = new TdfList("OIDS", 0);
		pss->mStruct["PJID"] = new TdfString("PJID", "123071");
		pss->mStruct["PORT"] = new TdfInteger("PORT", 8443);
		pss->mStruct["RPRT"] = new TdfInteger("RPRT", 9);
		pss->mStruct["TIID"] = new TdfInteger("TIID", 0);

		TdfStruct* tele = new TdfStruct("TELE");
		tele->mStruct["ADRS"] = new TdfString("ADRS", TELEMETRY_SERVER_ADDRESS);
		tele->mStruct["ANON"] = new TdfInteger("ANON", 0);

		// telemetryDisable
		tele->mStruct["DISA"] = new TdfString("DISA", "AD,AF,AG,AI,AL,AM,AN,AO,AQ,AR,AS,AW,AX,AZ,BA,BB,BD,BF,BH,BI,BJ,BM,BN,BO,BR,BS,BT,BV,BW,BY,BZ,CC,CD,CF,CG,CI,CK,CL,CM,CN,CO,CR,CU,CV,CX,DJ,DM,DO,DZ,EC,EG,EH,ER,ET,FJ,FK,FM,FO,GA,GD,GE,GF,GG,GH,GI,GL,GM,GN,GP,GQ,GS,GT,GU,GW,GY,HM,HN,HT,ID,IL,IM,IN,IO,IQ,IR,IS,JE,JM,JO,KE,KG,KH,KI,KM,KN,KP,KR,KW,KY,KZ,LA,LB,LC,LI,LK,LR,LS,LY,MA,MC,MD,ME,MG,MH,ML,MM,MN,MO,MP,MQ,MR,MS,MU,MV,MW,MY,MZ,NA,NC,NE,NF,NG,NI,NP,NR,NU,OM,PA,PE,PF,PG,PH,PK,PM,PN,PS,PW,PY,QA,RE,RS,RW,SA,SB,SC,SD,SG,SH,SJ,SL,SM,SN,SO,SR,ST,SV,SY,SZ,TC,TD,TF,TG,TH,TJ,TK,TL,TM,TN,TO,TT,TV,TZ,UA,UG,UM,UY,UZ,VA,VC,VE,VG,VN,VU,WF,WS,YE,YT,ZM,ZW,ZZ");
		tele->mStruct["FILT"] = new TdfString("FILT", "");
		tele->mStruct["LOC"] = new TdfInteger("LOC", 1701729619);

		// telemetryNoToggleOk
		tele->mStruct["NOOK"] = new TdfString("NOOK", "US,CA,MX");
		tele->mStruct["PORT"] = new TdfInteger("PORT", TELEMETRY_SERVER_PORT);

		tele->mStruct["SDLY"] = new TdfInteger("SDLY", 15000);
		tele->mStruct["SESS"] = new TdfString("SESS", "telemetry_session");
		tele->mStruct["SKEY"] = new TdfString("SKEY", "telemetry_key");
		tele->mStruct["SPCT"] = new TdfInteger("SPCT", 75);
		tele->mStruct["STIM"] = new TdfString("STIM", "Default");

		TdfStruct* tick = new TdfStruct("TICK");
		tick->mStruct["ADRS"] = new TdfString("ADRS", TICKER_SERVER_ADDRESS);
		tick->mStruct["PORT"] = new TdfInteger("PORT", TICKER_SERVER_PORT);

		// SKEY: id,ip:port,serviceName,helloperiod,bgred,bggreen,bgblue,bgalpha,hfilter,lfilter
		std::string skey = boost::lexical_cast<std::string>(userID) + "," + TICKER_SERVER_ADDRESS + ":" + boost::lexical_cast<std::string>(TICKER_SERVER_PORT)+"," + INSTANCE + ",10,50,50,50,50,0,0";
		tick->mStruct["SKEY"] = new TdfString("SKEY", skey);

		TdfStruct* urop = new TdfStruct("UROP");
		urop->mStruct["TMOP"] = new TdfInteger("TMOP", TELEMETRY_OPT_IN);
		urop->mStruct["UID"] = new TdfInteger("UID", userID);

		reply.WriteTdf(pss);
		reply.WriteTdf(tele);
		reply.WriteTdf(tick);
		reply.WriteTdf(urop);

		LOG_DEBUG("Sending reply to postAuth command.");

		return reply;
	}
}