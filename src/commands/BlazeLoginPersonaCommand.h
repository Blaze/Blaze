#pragma once

namespace Blaze
{
	class LoginPersonaCommand : public Command
	{
	private:
		unsigned int mServerTime;

	public:
		LoginPersonaCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}