#pragma once

namespace Blaze
{
	struct ClientInfo
	{
		TdfString* mEnvironment;
		TdfString* mPlatform;
		TdfString* mClientName;
		TdfString* mClientVersion;
		TdfString* mClientSkuID;
		unsigned int mClientLocale;
		TdfString* mBlazeSDKVersion;
		TdfString* mDirtySDKVersion;
		TdfString* mMacAddress;
	};

	struct ClientData
	{
		unsigned int mLocale;
		ClientType mClientType;
		bool mIgnoreInactivityTimeout;
		TdfString* mServiceName;
	};

	class PreAuthCommand : public Command
	{
	private:
		ClientInfo mClientInfo;
		ClientData mClientData;

	public:
		PreAuthCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}