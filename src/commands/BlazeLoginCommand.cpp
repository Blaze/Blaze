#include "StdInc.h"

namespace Blaze
{
	LoginCommand::LoginCommand()
		: Command(LOGIN_COMMAND)
	{
		
	}

	void LoginCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{
		mClientMail = (TdfString*)parsedData["MAIL"];

		//printf("lel %i\n", mClientMail->mLength);

		mClientPassword = (TdfString*)parsedData["PASS"];
	}

	Reply LoginCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		mServerTime = Utilities::GetUnixTime();

		Stream* pStream = new Stream(pFireFrame->mPayload, pFireFrame->mLength);

		TdfDecoder* pParser = new TdfDecoder();
		std::map<std::string, Tdf*> tdf = pParser->ReadData(pStream);

		ReadParsedData(tdf);

		int gameID = 2900; // rand() % 1000

		//printf("%s\n", mClientMail->c_str());

		//gameIDMap.insert(std::pair<std::string, int>(std::string("lel"), 1));
		
		int userID = 2900;

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		TdfStruct* persona1 = new TdfStruct("0");
		persona1->mStruct["DSNM"] = new TdfString("DSNM", "bf3-server-pc");
		persona1->mStruct["LAST"] = new TdfInteger("LAST", mServerTime);
		persona1->mStruct["PID"] = new TdfInteger("PID", userID);
		persona1->mStruct["STAS"] = new TdfInteger("STAS", 0);
		persona1->mStruct["XREF"] = new TdfInteger("XREF", 0);
		persona1->mStruct["XTYP"] = new TdfInteger("XTYP", 0);

		TdfList* plst = new TdfList("PLST", 3);
		plst->mList[0] = persona1;

		reply.WriteTdf(new TdfString("LDHT", ""));
		reply.WriteTdf(new TdfInteger("NTOS", 0));
		reply.WriteTdf(new TdfString("PCTK", "some_token_server"));
		reply.WriteTdf(plst);
		reply.WriteTdf(new TdfString("PRIV", ""));
		reply.WriteTdf(new TdfString("SKEY", "some_key_login"));
		reply.WriteTdf(new TdfInteger("SPAM", 1));
		reply.WriteTdf(new TdfString("THST", ""));
		reply.WriteTdf(new TdfString("TSUI", ""));
		reply.WriteTdf(new TdfString("TURI", ""));
		reply.WriteTdf(new TdfInteger("UID", userID));
		
		LOG_DEBUG("Sending reply to login command.");

		return reply;
	}
}