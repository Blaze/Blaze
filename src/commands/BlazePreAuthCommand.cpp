#include "StdInc.h"

namespace Blaze
{
	PreAuthCommand::PreAuthCommand()
		: Command(PREAUTH_COMMAND)
	{
		
	}

	void PreAuthCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{
		std::map<std::string, Tdf*> clientData = ((TdfStruct*)parsedData["CDAT"])->mStruct;
		std::map<std::string, Tdf*> clientInfo = ((TdfStruct*)parsedData["CINF"])->mStruct;
		//std::map<std::string, Tdf*> unknownInfo = ((TdfStruct*)parsedData["FCCR"])->mStruct;

		mClientData.mClientType = (ClientType)(((TdfInteger*)parsedData["TYPE"])->mInt);
		mClientData.mServiceName = (TdfString*)parsedData["SVCN"];

		mClientInfo.mBlazeSDKVersion = (TdfString*)parsedData["BSDK"];
		// BTIM time? string?
		// CLNT client? string?
		mClientInfo.mClientSkuID = (TdfString*)parsedData["CSKU"];
		mClientInfo.mClientVersion = (TdfString*)parsedData["CVER"];
		mClientInfo.mDirtySDKVersion = (TdfString*)parsedData["DSDK"];
		mClientInfo.mEnvironment = (TdfString*)parsedData["ENV"];
		mClientInfo.mClientLocale = (unsigned int)parsedData["LOC"];
		mClientInfo.mMacAddress = (TdfString*)parsedData["MAC"];
		//mClientInfo.mPlatform = (TdfString*)parsedData["PLAT"];

		//LOG_DEBUG("mClientData.mServiceName IS %s", mClientData.mServiceName->c_str());
	}

	TdfMap* GetLTPS()
	{
		TdfString* psa = new TdfString("PSA", "10.0.0.1");
		TdfInteger* psp = new TdfInteger("PSP", 17502);
		TdfString* sna = new TdfString("SNA", "ams");

		TdfStruct* ams = new TdfStruct("0"); // label was "ams"
		ams->mStruct["PSA"] = psa;
		ams->mStruct["PSP"] = psp;
		ams->mStruct["SNA"] = sna;

		TdfMap* ltps = new TdfMap("LTPS", 1, 3);
		ltps->mList1[0] = "ams";
		ltps->mList2[0] = ams;

		return ltps;
	}

	Reply PreAuthCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		BlazeClient* client = new BlazeClient();
		client->clientType = mClientData.mClientType;

		clientList.insert(std::make_pair(source.m_sadr, client));

		switch (mClientData.mClientType)
		{
		case CLIENT_TYPE_GAMEPLAY_USER:
			LOG_INFO("Received preAuth command from a client.");
			break;

		case CLIENT_TYPE_HTTP_USER:
			LOG_INFO("Received preAuth command from a HTTP user.");
			break;

		case CLIENT_TYPE_DEDICATED_SERVER:
			LOG_INFO("Received preAuth command from a dedicated server.");
			break;

		case CLIENT_TYPE_TOOLS:
			LOG_INFO("Received preAuth command from 'tools'.");
			break;

		case CLIENT_TYPE_INVALID:
			LOG_ERROR("Received invalid preAuth command.");
			break;
		}

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		TdfList* cids = new TdfList("CIDS", 0);
		cids->mList[0] = (void*)1;
		cids->mList[1] = (void*)25;
		cids->mList[2] = (void*)4;
		cids->mList[3] = (void*)27;
		cids->mList[4] = (void*)28;
		cids->mList[5] = (void*)6;
		cids->mList[6] = (void*)7;
		cids->mList[7] = (void*)9;
		cids->mList[8] = (void*)10;
		cids->mList[9] = (void*)11;
		cids->mList[10] = (void*)30720;
		cids->mList[11] = (void*)30721;
		cids->mList[12] = (void*)30722;
		cids->mList[13] = (void*)30723;
		cids->mList[14] = (void*)20;
		cids->mList[15] = (void*)30725;
		cids->mList[16] = (void*)30726;
		cids->mList[17] = (void*)2000;

		TdfMap* confList = new TdfMap("CONF", 1, 1);

		confList->mList1[0] = "connIdleTimeout";
		confList->mList2[0] = "90s";
		confList->mList1[1] = "defaultRequestTimeout";
		confList->mList2[1] = "80s";
		confList->mList1[2] = "pingPeriod";
		confList->mList2[2] = "20s";
		confList->mList1[3] = "voipHeadsetUpdateRate";
		confList->mList2[3] = "1000";
		confList->mList1[4] = "xlspConnectionIdleTimeout";
		confList->mList2[4] = "300";

		TdfStruct* confStruct = new TdfStruct("CONF");
		confStruct->mStruct["CONF"] = confList;

		TdfString* psa = new TdfString("PSA", "10.0.0.1");
		TdfInteger* psp = new TdfInteger("PSP", 17502);
		TdfString* sna = new TdfString("SNA", "ams");

		TdfStruct* bwps = new TdfStruct("BWPS");
		bwps->mStruct["PSA"] = psa;
		bwps->mStruct["PSP"] = psp;
		bwps->mStruct["SNA"] = sna;

		TdfStruct* qoss = new TdfStruct("QOSS");
		qoss->mStruct["BWPS"] = bwps;
		qoss->mStruct["LNP"] = new TdfInteger("LNP", 10);
		qoss->mStruct["LTPS"] = GetLTPS();
		qoss->mStruct["SVID"] = new TdfInteger("SVID", 1161889797);

		reply.WriteTdf(new TdfInteger("ANON", 0));
		reply.WriteTdf(new TdfString("ASRC", "300294"));
		reply.WriteTdf(cids);
		reply.WriteTdf(new TdfString("CNGN", ""));
		reply.WriteTdf(confStruct);
		reply.WriteTdf(new TdfString("INST", INSTANCE));
		reply.WriteTdf(new TdfInteger("MINR", 0));
		reply.WriteTdf(new TdfString("NASP", "cem_ea_id"));
		reply.WriteTdf(new TdfString("PILD", ""));
		reply.WriteTdf(new TdfString("PLAT", "pc"));
		reply.WriteTdf(new TdfString("PTAG", ""));
		reply.WriteTdf(qoss);
		reply.WriteTdf(new TdfString("RSRC", "300294")); // same as ASRC
		reply.WriteTdf(new TdfString("SVER", "Blaze 3.15.08.0 (CL# 834556)"));

		LOG_DEBUG("Sending reply to preAuth command.");

		return reply;
	}
}