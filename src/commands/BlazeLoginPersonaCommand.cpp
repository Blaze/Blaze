#include "StdInc.h"

namespace Blaze
{
	LoginPersonaCommand::LoginPersonaCommand()
		: Command(LOGINPERSONA_COMMAND)
	{
		
	}

	void LoginPersonaCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{
		
	}

	Reply LoginPersonaCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		mServerTime = Utilities::GetUnixTime();

		Stream* pStream = new Stream(pFireFrame->mPayload, pFireFrame->mLength);

		TdfDecoder* pReader = new TdfDecoder();
		std::map<std::string, Tdf*> tdf = pReader->ReadData(pStream);

		ReadParsedData(tdf);

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		int gameID = 2900;
		int userID = 2900;
		std::string mail = "bf3.server.pc@ea.com";
		std::string name = "bf3-server-pc";

		/*
		enum Blaze::Authentication::ExternalRefType::Code
		{
		BLAZE_EXTERNAL_REF_TYPE_UNKNOWN = 0x0,
		BLAZE_EXTERNAL_REF_TYPE_XBOX = 0x1,
		BLAZE_EXTERNAL_REF_TYPE_PS3 = 0x2,
		BLAZE_EXTERNAL_REF_TYPE_WII = 0x3,
		BLAZE_EXTERNAL_REF_TYPE_MOBILE = 0x4,
		BLAZE_EXTERNAL_REF_TYPE_LEGACYPROFILEID = 0x5,
		BLAZE_EXTERNAL_REF_TYPE_TWITTER = 0x6,
		BLAZE_EXTERNAL_REF_TYPE_FACEBOOK = 0x7,
		};
		*/

		TdfStruct* pdtl = new TdfStruct("PDTL");
		pdtl->mStruct["DSNM"] = new TdfString("DSNM", name);
		pdtl->mStruct["LAST"] = new TdfInteger("LAST", mServerTime);
		pdtl->mStruct["PID"] = new TdfInteger("PID", 2900);
		pdtl->mStruct["STAS"] = new TdfInteger("STAS", 0);
		pdtl->mStruct["XREF"] = new TdfInteger("XREF", 0);
		pdtl->mStruct["XTYP"] = new TdfInteger("XTYP", 0);

		reply.WriteTdf(new TdfInteger("BUID", 2900));
		reply.WriteTdf(new TdfInteger("FRST", 0));
		reply.WriteTdf(new TdfString("KEY", "85e940b5-59a3-4cdf-a322-701d6477f1ec"));
		reply.WriteTdf(new TdfInteger("LLOG", mServerTime));
		reply.WriteTdf(new TdfString("MAIL", mail));
		reply.WriteTdf(pdtl);
		reply.WriteTdf(new TdfInteger("UID", 2900));

		return reply;
	}
}