#pragma once

namespace Blaze
{
	class LoginCommand : public Command
	{
	private:
		TdfString* mClientMail;
		TdfString* mClientPassword;
		unsigned int mServerTime;

	public:
		LoginCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}