#include "StdInc.h"

namespace Blaze
{
	UpdateNetworkInfoCommand::UpdateNetworkInfoCommand()
		: Command(UPDATENETWORKINFO_COMMAND)
	{
		
	}

	void UpdateNetworkInfoCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{
		TdfUnion* addr = (TdfUnion*)parsedData["ADDR"];
		//TdfStruct* valu = (TdfStruct*)addr->mStruct["VALU"];
		//TdfStruct* inip = (TdfStruct*)valu->mStruct["INIP"];
		//printf("%i\n", ((TdfInteger*)inip->mStruct["IP"])->mInt);
		
		//for (std::map<std::string, Tdf*>::iterator iter = inip->mStruct.begin(); iter != inip->mStruct.end(); ++iter)
		{
			//was this the prob? yeah sec
			//printf("X: %s\n", iter->first);
		}

		//mInternalIP = (long)((TdfInteger*)inip->mStruct["IP@@"])->mInt;
		//mInternalPort = (unsigned short)((TdfInteger*)inip->mStruct["PORT"])->mInt;

		//mInternalIP = (long)((TdfInteger*)inip->mStruct["IP"])->mInt;
		
		//mInternalPort = (unsigned short)((TdfInteger*)inip->mStruct["PORT"])->mInt;
	}

	Reply UpdateNetworkInfoCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		//printf("\nhehe: %i\n\n", mInternalPort);
		//std::map<sockaddr_in, BlazeClient*>::iterator clientIterator = clientList.find(source.m_sadr);

		//if (clientIterator != clientList.end())
		{
		//	clientIterator->second->internalIP = mInternalIP;
		//	clientIterator->second->internalPort = mInternalPort;
		}

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		LOG_DEBUG("Sending reply to UpdateNetworkInfo command.");

		return reply;
	}
}