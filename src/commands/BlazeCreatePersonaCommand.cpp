#include "StdInc.h"

namespace Blaze
{
	CreatePersonaCommand::CreatePersonaCommand()
		: Command(CREATEPERSONA_COMMAND)
	{
		
	}

	void CreatePersonaCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{
		mPersonaName = "bf3-server-pc";
	}

	Reply CreatePersonaCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		//Database* db = new Database();

		//PersonaInfo personaInfo = db->CreatePersona("bf3-server-pc");

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		//printf("LOLOLOLO %i\n", personaInfo.id);
		//reply.WriteTdf(new TdfInteger("PID", personaInfo.id));

		//reply.WriteTdf(new TdfInteger("STIM", mServerTime));

		//LOG_DEBUG("Sending reply to CreatePersona command (PID: %i).", personaInfo.id);

		return reply;
	}
}