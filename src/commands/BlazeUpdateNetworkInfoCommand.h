#pragma once

namespace Blaze
{
	class UpdateNetworkInfoCommand : public Command
	{
	private:
		long mInternalIP;
		unsigned short mInternalPort;

	public:
		UpdateNetworkInfoCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}