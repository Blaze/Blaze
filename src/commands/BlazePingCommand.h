#pragma once

namespace Blaze
{
	class PingCommand : public Command
	{
	private:
		unsigned int mServerTime;

	public:
		PingCommand();

		void ReadParsedData(std::map<std::string, Tdf*> parsedData);
		Reply HandleData(FireFrame* pFireFrame, SocketObj source);
	};
}