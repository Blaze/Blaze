#pragma once

namespace Blaze
{
	class UserSessionExtendedDataUpdateCommand : public AsyncCommand
	{
	public:
		UserSessionExtendedDataUpdateCommand();

		Reply* CreateReply(SocketObj source);
	};
}