#pragma once

namespace Blaze
{
	class UserAddedCommand : public AsyncCommand
	{
	public:
		UserAddedCommand();

		Reply* CreateReply(SocketObj source);
	};
}