#include "StdInc.h"

namespace Blaze
{
	UserSessionExtendedDataUpdateCommand::UserSessionExtendedDataUpdateCommand()
		: AsyncCommand(USERSESSIONS_COMPONENT, USERSESSIONEXTENDEDDATAUPDATE_COMMAND)
	{
		
	}

	Reply* UserSessionExtendedDataUpdateCommand::CreateReply(SocketObj source)
	{
		Reply* reply = new Reply(mComponentID, mCommandID, 0, REPLY_ASYNC, 0);

		//int nat_type = NAT_TYPE_OPEN;
		long uatt = 123123;
		long longid = 2900;
		std::string name = "";

		long internal_ip = 0;
		unsigned short internal_port = 0;

		//std::map<sockaddr_in, BlazeClient*>::iterator clientIterator = clientList.find(source.m_sadr);

		//if (clientIterator != clientList.end())
		//{
		//	internal_ip = clientIterator->second->internalIP;
		//	internal_port = clientIterator->second->internalPort;
		//}

		TdfStruct* exip = new TdfStruct("EXIP");
		exip->mStruct["IP"] = new TdfInteger("IP", 3162594935);
		exip->mStruct["PORT"] = new TdfInteger("PORT", 25200);

		TdfStruct* inip = new TdfStruct("INIP");
		inip->mStruct["IP"] = new TdfInteger("IP", 3232235780);
		inip->mStruct["PORT"] = new TdfInteger("PORT", 25200);

		TdfStruct* valu = new TdfStruct("VALU");
		valu->mStruct["EXIP"] = exip;
		valu->mStruct["INIP"] = inip;

		TdfUnion* addr = new TdfUnion("ADDR", 2);
		addr->mStruct["VALU"] = valu;

		TdfMap* dmap = new TdfMap("DMAP", 0, 0);
		dmap->mList1[0] = (void*)458753;
		dmap->mList1[1] = (void*)458754;
		dmap->mList2[0] = (void*)82;
		dmap->mList2[1] = (void*)933;

		TdfList* pslm = new TdfList("PSLM", 0);
		pslm->mList[0] = (void*)268374015;
		pslm->mList[1] = (void*)268374015;
		pslm->mList[2] = (void*)268374015;
		pslm->mList[3] = (void*)268374015;
		pslm->mList[4] = (void*)268374015;

		TdfStruct* qdat = new TdfStruct("QDAT");
		qdat->mStruct["DBPS"] = new TdfInteger("DBPS", 0);
		qdat->mStruct["NATT"] = new TdfInteger("NATT", NAT_TYPE_UNKNOWN);
		qdat->mStruct["UBPS"] = new TdfInteger("UBPS", 0);

		TdfStruct* data = new TdfStruct("DATA");
		data->mStruct["ADDR"] = addr;
		data->mStruct["BPS"] = new TdfString("BPS", "ams");
		data->mStruct["CMAP"] = new TdfList("CMAP", 3);
		data->mStruct["CTY"] = new TdfString("CTY", "");
		data->mStruct["DMAP"] = dmap;
		data->mStruct["HWFG"] = new TdfInteger("HWFG", 0);
		data->mStruct["PLSM"] = pslm;
		data->mStruct["QDAT"] = qdat;
		data->mStruct["UATT"] = new TdfInteger("UATT", 0);
		//data->mStruct["ULST"] = new TdfList("ULST", 9);

		reply->WriteTdf(data);
		reply->WriteTdf(new TdfInteger("USID", 2900));

		return reply;
	}
}