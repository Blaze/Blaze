#include "StdInc.h"

namespace Blaze
{
	UserAddedCommand::UserAddedCommand()
		: AsyncCommand(USERSESSIONS_COMPONENT, USERADDED_COMMAND)
	{

	}

	Reply* UserAddedCommand::CreateReply(SocketObj source)
	{
		Reply* reply = new Reply(mComponentID, mCommandID, 0, REPLY_ASYNC, 0);

		//int nat_type = NAT_TYPE_OPEN;
		long uatt = 123123;
		long longid = 2900;
		std::string name = "";

		TdfMap* dmap = new TdfMap("DMAP", 0, 0);
		dmap->mList1[0] = (void*)458753;
		dmap->mList1[1] = (void*)458754;
		dmap->mList2[0] = (void*)82;
		dmap->mList2[1] = (void*)933;

		TdfStruct* qdat = new TdfStruct("QDAT");
		qdat->mStruct["DBPS"] = new TdfInteger("DBPS", 0);
		qdat->mStruct["NATT"] = new TdfInteger("NATT", NAT_TYPE_OPEN);
		qdat->mStruct["UBPS"] = new TdfInteger("UBPS", 0);

		TdfStruct* data = new TdfStruct("DATA");
		data->mStruct["ADDR"] = new TdfUnion("ADDR", 63);
		data->mStruct["BPS"] = new TdfString("BPS", "");
		//data->mStruct["CMAP"] = new TdfList("CMAP", 3);
		data->mStruct["CTY"] = new TdfString("CTY", "sadas");
		data->mStruct["DMAP"] = dmap;
		data->mStruct["HWFG"] = new TdfInteger("HWFG", 0);
		data->mStruct["PLSM"] = new TdfList("PLSM", 0);
		data->mStruct["QDAT"] = qdat;
		data->mStruct["UATT"] = new TdfInteger("UATT", 0);
		data->mStruct["ULST"] = new TdfList("ULST", 9);

		TdfStruct* user = new TdfStruct("USER");
		user->mStruct["AID"] = new TdfInteger("AID", longid);
		user->mStruct["ALOC"] = new TdfInteger("ALOC", Utilities::GetUnixTime());
		//user->mStruct["EXBB"] = new TdfStruct("EXBB");
		user->mStruct["EXID"] = new TdfInteger("EXID", 0);
		user->mStruct["ID"] = new TdfInteger("ID", longid);
		user->mStruct["NAME"] = new TdfString("NAME", "hehe");

		reply->WriteTdf(data);
		reply->WriteTdf(user);

		return reply;
	}
}