#include "StdInc.h"

namespace Blaze
{
	UserUpdatedCommand::UserUpdatedCommand()
		: AsyncCommand(USERSESSIONS_COMPONENT, USERUPDATED_COMMAND)
	{
		
	}

	Reply* UserUpdatedCommand::CreateReply(SocketObj source)
	{
		Reply* reply = new Reply(mComponentID, mCommandID, 0, REPLY_ASYNC, 0);

		long id = 2900;

		reply->WriteTdf(new TdfInteger("FLGS", 3));
		reply->WriteTdf(new TdfInteger("ID", id));

		return reply;
	}
}