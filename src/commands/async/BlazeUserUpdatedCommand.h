#pragma once

namespace Blaze
{
	class UserUpdatedCommand : public AsyncCommand
	{
	public:
		UserUpdatedCommand();

		Reply* CreateReply(SocketObj source);
	};
}