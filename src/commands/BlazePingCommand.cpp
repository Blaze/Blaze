#include "StdInc.h"
#include <time.h>

namespace Blaze
{
	PingCommand::PingCommand()
		: Command(PING_COMMAND)
	{
		mServerTime = 0;
	}

	void PingCommand::ReadParsedData(std::map<std::string, Tdf*> parsedData)
	{

	}

	Reply PingCommand::HandleData(FireFrame* pFireFrame, SocketObj source)
	{
		mServerTime = Utilities::GetUnixTime();

		Reply reply(mComponent->mComponentID, mCommandID, 0, REPLY_NORMAL, pFireFrame->mMsgNum);

		reply.WriteTdf(new TdfInteger("STIM", mServerTime));

		LOG_DEBUG("Sending reply to ping command (mServerTime = %i).", mServerTime);

		return reply;
	}
}