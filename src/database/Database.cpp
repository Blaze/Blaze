#include "StdInc.h"

Database::Database()
{
	db_info dbInfo = Config::GetDatabaseInfo();

	mHost = dbInfo.host;
	mUsername = dbInfo.username;
	mPassword = dbInfo.password;
	mDatabase = dbInfo.database;

	mDriver = get_driver_instance();
	mConnection = mDriver->connect(mHost.c_str(), mUsername.c_str(), mPassword.c_str());

	mConnection->setSchema(mDatabase.c_str());
}

Database::~Database()
{
	delete mResultSet;
	delete mStatement;
	delete mConnection;
}

bool Database::VerifyLogin(std::string email, std::string password)
{
	std::string query = "SELECT * FROM users WHERE email = \"" + email + "\" AND password = \"" + password + "\"";

	mStatement = mConnection->createStatement();
	mResultSet = mStatement->executeQuery(query.c_str());

	while (mResultSet->next())
	{
		if (mResultSet->rowsCount() > 0)
		{
			return true;
		}
	}

	return false;
}

PersonaInfo Database::GetPersona(std::string name)
{
	PersonaInfo personaInfo = PersonaInfo();

	personaInfo.id = 0;
	personaInfo.name = "";
	
	std::string query = "SELECT id AS _id, name AS _name FROM personas WHERE name = \"" + name + "\"";

	mStatement = mConnection->createStatement();
	mResultSet = mStatement->executeQuery(query.c_str());

	while (mResultSet->next())
	{
		if (mResultSet->rowsCount() > 0)
		{
			personaInfo.id = mResultSet->getInt("_id");
			personaInfo.name = name;
			//personaInfo.name = std::string(mResultSet->getString("_name"));
		}
	}

	return personaInfo;
}

PersonaInfo Database::CreatePersona(std::string name)
{
	if (GetPersona(name).id != 0)
	{
		return GetPersona(name);
	}
	else
	{
		sql::PreparedStatement *prep_stmt;
		prep_stmt = mConnection->prepareStatement("INSERT INTO personas (name) VALUES(?)");

		prep_stmt->setString(1, name.c_str());
		prep_stmt->execute();

		delete prep_stmt;
	}

	return GetPersona(name);
}