#pragma once

struct PersonaInfo
{
	int id;
	std::string name;
};

class Database
{
private:
	std::string mHost;
	std::string mUsername;
	std::string mPassword;
	std::string mDatabase;

	sql::Driver *mDriver;
	sql::Connection *mConnection;
	sql::Statement *mStatement;
	sql::ResultSet *mResultSet;
public:
	Database();
	~Database();

	bool VerifyLogin(std::string email, std::string password);

	PersonaInfo GetPersona(std::string name);
	PersonaInfo CreatePersona(std::string name);
};