#include "StdInc.h"

namespace Blaze
{
	HubServer::HubServer()
		: ServerBase(42127)
	{
		AddComponent(new RedirectorComponent());
	}
}