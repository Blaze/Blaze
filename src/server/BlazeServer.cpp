#include "StdInc.h"

namespace Blaze
{
	Server::Server()
		: ServerBase(10981)
	{
		AddComponent(new UtilComponent());
		AddComponent(new AuthenticationComponent());
		AddComponent(new UserSessionsComponent());
	}
}