#include "StdInc.h"

namespace Blaze
{
	UtilComponent::UtilComponent()
		: Component(UTIL_COMPONENT)
	{
		AddCommand(new PingCommand());
		AddCommand(new PreAuthCommand());
		AddCommand(new PostAuthCommand());
		//AddCommand(new FetchClientConfigCommand());
		//AddCommand(new GetTelemetryServerCommand());
	}
}