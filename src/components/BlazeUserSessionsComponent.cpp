#include "StdInc.h"

namespace Blaze
{
	UserSessionsComponent::UserSessionsComponent()
		: Component(USERSESSIONS_COMPONENT)
	{
		AddCommand(new UpdateNetworkInfoCommand());
		//AddCommand(new GetTelemetryServerCommand());
	}
}