#include "StdInc.h"

namespace Blaze
{
	AuthenticationComponent::AuthenticationComponent()
		: Component(AUTHENTICATION_COMPONENT)
	{
		AddCommand(new LoginCommand());
		AddCommand(new CreatePersonaCommand());
		AddCommand(new LoginPersonaCommand());
	}
}