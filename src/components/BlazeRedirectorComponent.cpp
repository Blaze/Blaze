#include "StdInc.h"

namespace Blaze
{
	RedirectorComponent::RedirectorComponent()
		: Component(REDIRECTOR_COMPONENT)
	{
		AddCommand(new GetServerInstanceCommand());
	}
}