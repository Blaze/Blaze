solution "BlazeServer"
	configurations { "Debug", "Release" }

	flags { "Unicode", "Symbols" }

	includedirs { "deps/include/" }

	libdirs { "deps/lib/" }

	configuration "Debug*"
		targetdir "bin/debug"

	configuration "Release*"
		targetdir "bin/release"

	project "BlazeServer"
		targetname "BlazeServer"
		language "C++"
		kind "ConsoleApp"

		links
		{
			"ws2_32.lib",
			"libboost_system-vc120-mt-gd-1_58.lib",
			"libboost_filesystem-vc120-mt-gd-1_58.lib",
			"mysqlcppconn.lib",
			"mysqlcppconn-static.lib"
		}

		includedirs { "deps/include/yaml-cpp", "deps/include/mysql", "src/shared" }

		files
		{
			"src/**.cpp",
			"src/**.h"
		}

		pchsource "src/shared/StdInc.cpp"
		pchheader "StdInc.h"

		prebuildcommands
		{
			"pushd \"$(SolutionDir)\\deps\\tools\"",
			"call \"$(SolutionDir)\\deps\\tools\\gitrev.cmd\"",
			"popd"
		}

		configuration "Debug*"
			links { "libyaml-cppmdd.lib" }

		configuration "Release*"
			links { "libyaml-cppmd.lib" }